import { Component } from '@angular/core';
import { HttpService } from './services/http.service';
import { Title }     from '@angular/platform-browser';
import { WindowReferenceService } from './services/windowreference.service';

@Component({
  	selector: 'app-root',
  	templateUrl: './app.component.html',
  	styleUrls: ['./app.component.css'],
	providers: [
		HttpService,
		WindowReferenceService
	]
})
export class AppComponent {
	public constructor(private titleService: Title ) { 
		this.titleService.setTitle('InveStation');
	}
}
