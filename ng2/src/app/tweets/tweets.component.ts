import { Component, OnInit } from '@angular/core';
import { HttpService } from '../services/http.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { WindowReferenceService } from '../services/windowreference.service';

@Component({
  selector: 'app-tweets',
  templateUrl: './tweets.component.html',
  styleUrls: ['./tweets.component.css']
})

export class TweetsComponent implements OnInit {

	private sub : any;
	private winRef : any = null;

	tweets : any[];
	replies : any[];
	analysis : any[];
	id_str : string;
	bShowLoader : boolean = true;
    isShowMenu: boolean = true;

	constructor(private HttpService: HttpService, private windowReferenceService: WindowReferenceService, private activatedRoute: ActivatedRoute) {
		this.winRef = windowReferenceService.nativeWindow;
		this.id_str = null;
	}

	ngOnInit() {
		this.activatedRoute.params.subscribe((params: Params) => {
	      this.getTweets(params.params, params.sentiment);
	    });

	}

	createTweets(tweet, elementId, last)
	{
		if(tweet.bRendered !== true){
			this.winRef.twttr.ready(() => {

				let container = document.getElementById(elementId+'-'+tweet.id_str);

				tweet.bRendered !== true &&
				this.winRef.twttr.widgets.createTweet( tweet.id_str, container )
				.then(element => {

					let embeddedTweet = <HTMLElement> document.querySelector('#' + elementId + '-' + tweet.id_str + '.panel .panel-title');
					let twitterwidget = <HTMLElement> document.querySelector('#' + elementId + '-' + tweet.id_str + ' twitterwidget');
					let loader = <HTMLElement> document.querySelector('#' + elementId + '-' + tweet.id_str + ' .loader');

					if(embeddedTweet) {
						embeddedTweet.innerHTML = tweet.analysis;
						loader.style.visibility = 'hidden';
					}

					container && (container.style.display = twitterwidget.style.visibility == 'hidden' ? 'none' : 'inline-block');
				});
				tweet.bRendered = true;
			});
		}

	}

	getTweets(key,sentiment) {
		this.HttpService.getTweets(key,sentiment)
			.subscribe( data => {
				if(data[0]['aItem']['tweets'] ) {
					this.tweets = JSON.parse(JSON.stringify(data[0]['aItem']['tweets']));
				}

				this.hideLoader();

				console.log(this.tweets);
			});
	}

	ToggleModal(state)
	{
		console.log(this.analysis);
		// let display = document.getElementById('replies_modal').style.display;
		document.getElementById('replies_modal').style.display = state;
	}

	hideLoader()
	{
		this.bShowLoader = false;
	}

	sentimenBorderColor( tweet ) {
		if(tweet.analysis != '') {
			return tweet.analysis == 'pos' ? 'green' : 'red';
		} else {
			return 'gray';
		}

	}

    showMenu(){
      this.isShowMenu = !this.isShowMenu;
    }
}
