import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { TweetsComponent } from './tweets/tweets.component';

 const appRoutes : Routes = [
 	{
 		path: '',
 		component: HomeComponent
 	},
 	{
 		path: 'tweets',
 		component: TweetsComponent
 	},
    {
 		path: 'tweets/:params/:sentiment',
 		component: TweetsComponent
 	}
 ]

 export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
