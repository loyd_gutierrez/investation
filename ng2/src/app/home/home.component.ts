import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from '../services/http.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  public sentimentPieChartLabels:string[] = [
    'education',
    'economy',
    'resources',
    'climate',
    'location',
    'government',
    'culture',
    'population'
  ];

  public tweetsPieChartLabels:string[] = [
    'negative',
    'positive'
  ];

  public negativePieChartData:number[] = [1,1,1,1,1,1,1,1];
  public positivePieChartData:number[] = [1,1,1,1,1,1,1,1];
  public tweetsPieChartData:number[] = [1,1];

  public pieChartType:string = 'pie';
  public legends: any[];
  public tweets : any[] = [];

  title : string;
  aTeams : any[];
  isShowMenu: boolean = true;

  constructor(private HttpService: HttpService, private router: Router) {}

	ngOnInit() {
        this.title = 'InveStation';

        this.aTeams = [
        	{ sName : 'Alex On Lojo', username: 'mayhem_blue', img: '640810037317767168/MUZ98Jag_400x400.jpg' },
        	{ sName : 'Derick Bautista', username: 'IncredibleDrK', img: '858197538792394752/5q4GAmZ__400x400.jpg' },
        	{ sName : 'Kristian Dave Cruz Pelegrino', username: 'kdpelegrino', img: '886811162574168064/CNx43M6e_400x400.jpg' },
        ];

        this.legends = [
          { title: 'education' , color: '#ffa2b5' },
          { title: 'economy', color: '#8bc7f3'},
          { title: 'resources', color: '#ffe29d'},
          { title: 'climate', color: '#f1f2f4'},
          { title: 'location', color: '#98dad9'},
          { title: 'government', color: '#c1d6e1'},
          { title: 'culture', color: '#eaeaea'},
          { title: 'population', color: '#fd9393'}
        ];

        this.HttpService.getParameters()
          .subscribe( data => {
            if(data[0]['aItem']) {
              this.tweets = JSON.parse(JSON.stringify(data[0]['aItem']));

              this.positivePieChartData = this.setParamsData(this.tweets['params'].POSITIVE);
              this.negativePieChartData = this.setParamsData(this.tweets['params'].NEGATIVE);

              this.tweetsPieChartData = [ this.tweets['neg'], this.tweets['pos'] ];
            }
          }
        );

	}

  private setParamsData(params) {
    return [ params.education, params.economy, params.resources, params.climate, params.location, params.government, params.culture, params.population ]
  }

  public posSentimentPieChartClicked(e:any):void {
    this.searchTweet(this.sentimentPieChartLabels[e.active[0]._index], 'POSITIVE');
  }

  public negSentimentPieChartClicked(e:any):void {
    this.searchTweet(this.sentimentPieChartLabels[e.active[0]._index],'NEGATIVE');
  }

  public totalPieChartClicked(e:any):void {
    this.searchTweet('',(this.tweetsPieChartLabels[e.active[0]._index]).toUpperCase());
  }

  getParametersPercentage(a,b,total) {
    return ((this.tweets['params'][a][b] / total) * 100).toFixed(2) + '%';
  }

  searchTweet( params, sentiment ) {
    this.router.navigate(['/tweets',params, sentiment]);
  }

  showMenu(){
    this.isShowMenu = !this.isShowMenu;
  }
}
