import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class HttpService {
  	constructor(private http : Http) {
  		console.log(location.hostname);
  	}

  	getTweets(params: string = null, sentiment : string = null) {
      let api = 'http://'+  location.hostname +'/investation/api/tweets';
  		return this.http.get(api+ (sentiment || params ? '?key='+params+'&sentiment='+sentiment : ''))
  			.map(res => res.json());
  	}

    getParameters() {
      console.log('http://'+  location.hostname +'/investation/api/tweets');
  		let api = 'http://'+  location.hostname +'/investation/api/tweets';
  		return this.http.get(api+ '?paramsOnly=true')
  			.map(res => res.json());
  	}
}
