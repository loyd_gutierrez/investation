<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Certificates extends CI_Model
{
    function __construct()
    {
        parent::__construct();

        $this->load->library('tools/tools_mpdf', array(
			'mode'	=> 'c'
		));
    }

	public function print_certificate($sModule, $aArg = array())
	{
		if (file_exists(FCPATH.'assets/css/pdf/reports/libs/'.$sModule.'.css')) {
			$this->tools_mpdf->WriteHTML(file_get_contents(FCPATH.'assets/css/pdf/reports/libs/'.$sModule.'.css'), 1);
		}

		$this->tools_mpdf->WriteHTML(file_get_contents(FCPATH.'assets/css/pdf/reports/core.css'), 1);
		$this->tools_mpdf->WriteHTML($this->load->view('certificates/'.$sModule, $aArg, TRUE));

		return array(
			'iErr'	=> 0,
			'sMsg'	=> 'Success',
			'aData'	=> array(
				'sB64Pdf'	=> base64_encode($this->tools_mpdf->Output('', 'S'))
			)
		);
	}
}