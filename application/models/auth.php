<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends Api
{

	function __construct()
	{
		parent::__construct();
		$this->load->library('users.php');
	}

	function info(){
		return array(
			'iUserId'   	=> $this->tools_sessions->get('iUserId'),
			'username'  	=> $this->tools_sessions->get('username'),
			'sFullName'  	=> $this->tools_sessions->get('sFullName'),
			'sRole'  		=> $this->tools_sessions->get('sRole'),
			'sPhoto'  		=> $this->tools_sessions->get('sPhoto'),
		);		
	}

	function login($aArg = array())
	{
		print_r($aArg);
		exit();
		if(!$aArg) return;

		$aData = $this->users->get(
			array(
				'sEmail' => $aArg['sEmail'],
				'sPassword' => md5($aArg['sPassword']),
				'bActive'	=> 1
			)
		);

		if($aData['data'])
		{
			$this->tools_sessions->set(
				array(
					'aInfo' => array(
						'iUserId'   	=> $aData['data'][0]['iUserId'],
						'sUserName'  	=> $aData['data'][0]['sUserName'],
						'sName'  		=> $aData['data'][0]['sName'],
						'sRole'  		=> $aData['data'][0]['sRole'],
						'sPhoto'  		=> $aData['data'][0]['sPhoto'],
						'iDateTime' 	=> strtotime(date('Y-m-d'))
					)
				)
			);

			return array(
				'iUserId'  		=> $aData['data'][0]['iUserId'],
				'sUserName'  	=> $aData['data'][0]['sUserName'],
				'sName'  		=> $aData['data'][0]['sName'],
				'sRole'  		=> $aData['data'][0]['sRole'],
				'sContactNumber'=> $aData['data'][0]['sContactNumber'],
				'sAddress'  	=> $aData['data'][0]['sAddress'],
				'sPhoto'  		=> $aData['data'][0]['sPhoto']
			);
		}
		else
		{
			return array(
				'bNotify'  => TRUE,
				'iError'   => 1004,
				'sMessage' => 'Username and Password did not match'
			);
		}
	}

	function logout()
	{	
		$this->tools_sessions->logout();
		return array(
				'sMessage' => 'success'
			);
	}
}