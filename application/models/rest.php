<?php defined('BASEPATH') OR exit('No direct script access allowed');

class REST extends CI_Model
{
	function __construct()
    {
        parent::__construct();

    }

    public function get($sModule, $sId = null)
    {
        $aRequest = null;
        $aData = [];
        $_REQUEST && $aRequest = json_decode(base64_decode(array_keys($_REQUEST)[0],TRUE),TRUE);

        if($aRequest && $aRequest['aRequests']) {
            foreach($aRequest['aRequests'] as $iKey => $aRow){
                $sTemp = $this->loadModule($aRow['sModule']);

                array_push($aData,$this->$sTemp->get(null,$aRow));
            }
        }
        
        $sModule = $this->loadModule($sModule);
        $aTemp = $this->$sModule->get($sId,$aRequest);

        array_push($aData,[
                'aItem' => isset($aTemp['data']) ? $aTemp['data'][0] : $aTemp
            ]
        );

        return $sModule == 'db_main_modules' ? array_merge(['aContents'     => $this->assets($this->db_main_modules->get())],$aData) : $aData;
    }

    /** For Add and Login */
    public function post($sModule = null, $sId = null, $aRequest = [])
    {
        if(!$sModule) return array(
            'iError'   => 401.4,
            'sMessage' => 'Authorization failed'
        );
        if($sModule == 'auth' || $sModule == 'logout') return $this->$sModule($aRequest);

        $sModule = $this->loadModule($sModule);
        return $this->$sModule->add($sId,$aRequest);
    }	

    /** For Update */
    public function put($sModule = null, $sId = null, $aRequest = [])
    {

        if(!$sModule || !$sId) return array(
            'iError'   => 401.4,
            'sMessage' => 'Authorization failed by filter'
        );

        $sModule = $this->loadModule($sModule);
        return $this->$sModule->edit($sId,$aRequest);
    }

    /** For Delete */
    public function delete($sModule, $sId, $aRequest = [])
    {
    	//module and id is required
    }

    /** Login */
    public function auth($aData = [])
    {
        $this->load->library('db/main/db_main_users');
        $this->load->library('db/main/db_main_modules');
        $aData['sPassword'] = md5($aData['sPassword']);

        $aUser = $this->db_main_users->get(null,$aData);
        $aContents = [];

        if($aUser['data'])
        {
            $this->tools_sessions->set(
                array(
                    'aInfo' => array(
                        'iUserId'       => $aUser['data'][0]['iUserId'],
                        'sEmail'     => $aUser['data'][0]['sEmail'],
                        'sFullName'         => $aUser['data'][0]['sFullName'],
                        'sRole'         => $aUser['data'][0]['sRole'],
                        'sPhoto'     => $aUser['data'][0]['sPhoto'],
                        'iDateTime'     => strtotime(date('Y-m-d'))
                    )
                )
            );

            return array(
                'iUserId'       => $aUser['data'][0]['iUserId'],
                'sEmail'        => $aUser['data'][0]['sEmail'],
                'sFullName'     => $aUser['data'][0]['sFullName']
            );
        }
        else
        {
            return array(
                'bNotify'  => TRUE,
                'iError'   => 401.1,
                'sMessage' => 'Username and Password did not match'
            );
        }
    }

    private function logout()
    {
        $this->tools_sessions->logout();
        return [
            'sMessage' => 'success'
        ];
    }

    /**
     * Get's All module asset files.
     */
    private function assets($aArg = array())
    {
        if(!$aArg) return null;

        $aContent = array();
        
        foreach($aArg['aModules'] as $iKey => $aRows){
            $aRows['bList'] && file_exists('application/views/modules/lists/lists_'. strtolower($aRows['sName']) .'.php') &&
                $aContent['lists'][ $aRows['sName'] ] = $this->load->view( 'modules/lists/lists_'. strtolower($aRows['sName']),null,true);

            $aRows['bForm'] && file_exists('application/views/modules/forms/forms_'. strtolower($aRows['sName']) .'.php') &&
                $aContent['forms'][ $aRows['sName'] ] = $this->load->view( 'modules/forms/forms_'. strtolower($aRows['sName']),null,true);

            file_exists(PATH_ASSETS_JS.'modules/'.strtolower($aRows['sName']).'.js') &&
                ($aContent['js'][ $aRows['sName'] ] = file_get_contents(PATH_ASSETS_JS.'modules/'.strtolower($aRows['sName']).'.js'));


            if(isset($aRows['aChildren'])) {
                
                $aChildren = $this->assets(array( 'aModules' => $aRows['aChildren'])); 

                $aContent['forms'] = array_merge($aContent['forms'],$aChildren['forms']);
                $aContent['lists'] = array_merge($aContent['lists'],$aChildren['lists']);
                $aContent['js'] = array_merge($aContent['js'],$aChildren['js']);
            }
        }

        return array_merge($aArg,$aContent);
    }

    protected function loadModule($sModule){
        $sModule = 'db_main_'.strtolower($sModule);
        if(!class_exists($sModule)) $this->load->library('db/main/'.$sModule.'.php');
        return $sModule;
    }
    /**
     * convert's data being passed to be readable by library classes
     * @return object
     */
    public function parse($aData)
    {
        // return $aData;
    }
}