<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>InveStation</title>
  <base href="/investation/">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="assets/dist/styles.ba83f88d9663e900e074.bundle.css" rel="stylesheet"/>
  <link rel="icon" type="image/x-ico" href="assets/images/favicon.ico">
  <link rel="icon" type="image/ico" href="assets/images/favicon.ico">
</head>
<body>
	<app-root>Loading...</app-root>
	<script>
		// START twttr initialization
		window.twttr = (function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0],
		  t = window.twttr || {};
		  if (d.getElementById(id)) return t;
		  js = d.createElement(s);
		  js.id = id;
		  js.src = "https://platform.twitter.com/widgets.js";
		  fjs.parentNode.insertBefore(js, fjs);

		  t._e = [];
		  t.ready = function(f) {
		    t._e.push(f);
		  };

		  return t;
		}(document, "script", "twitter-wjs"));
		// END twttr initialization
	</script>

	<script type="text/javascript" src="assets/dist/inline.bundle.js"></script>
	<script type="text/javascript" src="assets/dist/polyfills.bundle.js"></script>
	<script type="text/javascript" src="assets/dist/scripts.bundle.js"></script>
	<script type="text/javascript" src="assets/dist/styles.bundle.js"></script>
	<script type="text/javascript" src="assets/dist/vendor.bundle.js"></script>
	<script type="text/javascript" src="assets/dist/main.bundle.js"></script>
  </body>
</html>
