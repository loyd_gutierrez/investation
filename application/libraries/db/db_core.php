<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @name        db core controller
 * @author      Lloyd Gutierrez <jlgutierrez24@gmail.com>
 * @version		1.0.0
 */

class db_core
{
	var $CI			= NULL;

	public function __construct()
	{
		$this->CI =& get_instance();
	}

	/**
	 * [select description]
	 * @param  array  $aArg [description]
	 * @return [type]       [description]
	 */
	
	protected function select($aArg = array(), $aColumns = array())
	{
		$aSelect = array();
		if(count($aArg) > 0) {
			foreach($aArg as $iKey => $aRows){
				if( isset($aRows['data']) && in_array($aRows['data'],array_keys($aColumns))) array_push($aSelect, $aColumns[$aRows['data']] );
			}
		}

		return implode( ',', count($aSelect) > 0 ? array_values($aSelect) : array_values($aColumns) );
	}

	protected function construct($aArg = array(),$sColumns = array())
	{

		//array_merge(array_flip($aArg['aColumns']),$this->aColumns)
	}
	
	protected function where($aArg = array(),$sKey, $sTable = NULL)
	{
		if(!isset($aArg[$sKey])) return;
		
		$mKey = $aArg[$sKey];
		$sAction = is_array($mKey) ? 'where_in' : 'where';

		$this->CI->db->$sAction($sTable.'.'.$sKey,$mKey);
	}

	/**
	 * [order description]
	 * @param  array  $aArg [description]
	 * @return [type]       [description]
	 */
	protected function order($aArg = array())
	{
		if($aArg['aOrder']) foreach($aArg['aOrder'] as $iKey => $aRow){
			$sColumn = $this->aColumns[$aRow[0]]; 

			if(strpos($sColumn,'CONCAT') > -1){
				$sColumn = array_pop(explode(' ', $this->aColumns[$aRow[0]]));
			}

			if(isset($this->aColumns[$aRow[0]])) $this->CI->db->order_by($sColumn.' '.$aRow[1]);
		}
		// isset($aArg['aOrder']) && sizeof($aArg['aOrder']) > 0 ? 
	}
	/**
	 * [order description]
	 * @param  array  $aArg [description]
	 * @return [type]       [description]
	 */
	protected function group($aArg = array())
	{
		isset($aArg['aGroup']) && $this->CI->db->group_by(is_array($aArg['aGroup']) ? implode(', ', $aArg['aGroup']) : $aArg['aGroup']);
	}

	public function limit($aArg)
	{
		isset($aArg['iLimit']) && $this->CI->db->limit($aArg['iLimit'], (
			isset($aArg['iOffset']) ?
			$aArg['iOffset'] : (
				isset($aArg['iPage']) ?
				($aArg['iPage'] - 1) * $aArg['iLimit']:
				0
			)
		));
	}
	
	protected function search($aArg = array(), $sTable = null)
	{
		if (!isset($this->aColumns) || !isset($aArg['sSearch']) || !$aArg['sSearch']) return;

		$aSearch		= explode(' ', $aArg['sSearch']);
		$aColumns		= array_intersect_key($this->aColumns, array_flip($aArg['aColumns']));
		$aAndFilters	= array();

		foreach ($aSearch as $iIndex => $sWord) {
			$aOrFilters	= array();
			foreach ($aColumns as $sColumn) {
				if(strpos($sColumn,'CONCAT') > -1){
					$sColumn = array_pop(explode(' ', $sColumn));
				}

				$aOrFilters[] = $sColumn.' LIKE "%'.$this->CI->db->escape_like_str($sWord).'%"';
			}
			$aAndFilters[]	= '('.implode(' OR ', $aOrFilters).')';
		}

		$this->CI->db->having( implode(' AND ', $aAndFilters) );
		// $this->CI->db->ar_where	[] = (
		// 	(count($this->CI->db->ar_where) ? ' AND ' : '').		
		// 	'('.implode(' AND ', $aAndFilters).')'
		// );
	}

	/**
	 * Compose the result from query
	 * @param  array  $aRes : Query result
	 * @param  array  $aArg : Request
	 * @return array        : return depends on request format
	 */
	protected function getResult($aRes = array(),$aArg = array())
	{
		$aReturn = array();
		
		foreach($aRes as $aRows)
		{
			if(isset($aArg['sColumn'])) 
			{	
				if(isset($aArg['sIndex'])) $aReturn[$aRows[$aArg['sIndex']]] = $aRows[$aArg['sColumn']];
				else $aReturn[] = $aRows[$aArg['sColumn']];
			}
			else
			{
				if(isset($aArg['sIndex'])) $aReturn[$aRows[$aArg['sIndex']]] = $aRows;
				else $aReturn[] = $aRows;
			}
		}
		
		return $aReturn;
	}

	/**
	 * Gets row count and all the filtered rows of the last query 
	 * @return Array 	: total rows and filtered rows
	 */
	protected function getFoundRows()
	{
		$aRows = array();

		$oQuery			= $this->CI->db->query('SELECT FOUND_ROWS() iTotalRows');
		$aTotalRows		= $oQuery->first_row();
		$oQuery->free_result();

		$oQuery			= $this->CI->db->query('SELECT FOUND_ROWS() iFilteredRows');
		$aFilteredRows	= $oQuery->first_row();

		return array( 'recordsTotal' => $aTotalRows->iTotalRows, 'recordsFiltered' => $aFilteredRows->iFilteredRows );
	}

	protected function insert($aArg = array())
	{
		$this->CI->db->insert(
			$this->sTable,
			array_intersect_key(
				$aArg,
				array_flip($this->aFields)
			)
		);

		return array('id' => $this->CI->db->insert_id());
	}

	protected function update($aArg = array())
	{	
		if(isset($aArg[$this->sIndex])) unset($aArg[$this->sIndex]);
		
		if( count(array_intersect_key($aArg,array_flip($this->aFields))) == 0) return ;

        $this->CI->db->update(
        	$this->sTable,
        	array_intersect_key($aArg,array_flip($this->aFields))
		);
	}

	protected function delete($aArg = array())	
	{
		$this->CI->db->delete($this->sTable);
	}
}
