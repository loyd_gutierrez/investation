<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_main_users extends Db_core
{
	
	public $CI 			= null;

	protected $sModule  = '';
	protected $sTable 	= 'users';
	protected $sIndex 	= 'iUserId';

	protected $aColumns = array(
		'iUserId'				=> 'users.iUserId',

		'iDob'					=> 'users.iDob',
		'sEmail'				=> 'users.sEmail',
		'iNationalityId'		=> 'users.iNationalityId',
		'sLastName'				=> 'users.sLastName',
		'sFirstName'			=> 'users.sFirstName',
		'sMiddleName'			=> 'users.sMiddleName',
		'sGender'				=> 'users.sGender',
		'sBloodType'			=> 'users.sBloodType',
		'sPhoto'				=> 'users.sPhoto',
		'sTinId'				=> 'users.sTinId',
		'sMobileNumber_Residencial'	=> 'users.sMobileNumber_Residencial',
		'sMobileNumber_Business'	=> 'users.sMobileNumber_Business',

		'sRecidencialAddress'	=> 'users.sRecidencialAddress',
		'sBusinessAddress'		=> 'users.sBusinessAddress',

		'iCreatedAt'			=> 'users.iCreatedAt',
		'iUpdatedAt'			=> 'users.iUpdatedAt',
		
		'sNationality'			=> 'nationalities.sNationality',
		'sFullName'				=> 'CONCAT( users.sLastName, CONCAT(" "), users.sFirstName ) sFullName',
		'sRole'					=> 'roles.sRole',

		'iResidencialZipCodeId' => 'residencial_address.iZipCodeId iResidencialZipCodeId',
		'sResidencial_Province'	=> 'residencial_address.sProvince sResidencial_Province',
		'sResidencial_Municipality'=> 'residencial_address.sMunicipality sResidencial_Municipality',
		'sResidencial_District'	=> 'residencial_address.sDistrict sResidencial_District',
		'sResidencial_sZipCode' => 'residencial_address.sZipCode sResidencial_sZipCode',

		'iBusinessZipcodeId' 	=> 'business_address.iZipCodeId iBusinessZipcodeId',
		'sBusiness_Province'	=> 'business_address.sProvince sBusiness_Province',
		'sBusiness_Municipality'=> 'business_address.sMunicipality sBusiness_Municipality',
		'sBusiness_District'	=> 'business_address.sDistrict sBusiness_District',
		'sBusiness_sZipCode'	=> 'business_address.sZipCode sBusiness_sZipCode',
	);

	protected $aFields = array(
		'iUserId',
		'bActive',
		'iRoleId',
		'iDob',
		'sEmail',
		'sPassword',
		'iNationalityId',
		'sLastName',
		'sFirstName',
		'sMiddleName',
		'sGender',
		'sPhoto',
		'sRecidencialAddress',
		'sBusinessAddress',
		'sMobileNumber_Residencial',
		'sMobileNumber_Business',
		'iResidencialZipCodeId',
		'iBusinessZipcodeId',
		'sBloodType',
		'sTinId'
	);

	function __construct(){
		$this->CI =& get_instance();
	}

	public function get($sId = null ,$aArg = array())
	{
		$this->CI->db->select($this->select($aArg, array_merge(
			$this->aColumns,
			array(
				'bACtive'				=> 'users.bActive',
				'iUserId'				=> 'users.iUserId',
				'iRoleId'				=> 'users.iRoleId',
			)
		)));

		$this->CI->db->from($this->sTable);

		$this->CI->db->join('roles','roles.iRoleId = users.iRoleId','left');
		$this->CI->db->join('nationalities','nationalities.iNationalityId = users.iNationalityId','left');
		$this->CI->db->join('zip_codes residencial_address','residencial_address.iZipCodeId = users.iResidencialZipCodeId','left');
		$this->CI->db->join('zip_codes business_address','business_address.iZipCodeId = users.iBusinessZipcodeId','left');

		$this->where($aArg, 'sEmail','users');
		$this->where($aArg, 'sPassword','users');
		// $this->where($aArg, 'iUserId','users');

		$sId && $this->CI->db->where('users.iUserId = '.$sId);
		if(isset($aArg['iVesselOwnerId'])) $this->CI->db->where('users.iUserId = '.$aArg['iVesselOwnerId']);

		$this->order($aArg);
		$this->limit($aArg);
		
		$this->search($aArg);

		$aData 		= $this->CI->db->get();
		$aReturn 	= $aData->result_array();

		return array_merge(
			$this->getFoundRows(),
			array('data' => $this->getResult($aReturn,$aArg))
		);
	}

	public function add($id = null, $aArg)
	{
		$aArg = $this->password($aArg);

		$aArg['aRequest']['sPhoto'] = $this->image($aArg);
		$aId = $this->insert($aArg['aRequest']);

		return array('sMessage' => 'Record saved!', 'iUserId' => $aId['id']);
	}

	public function edit($id, $aArg = array())
	{
		$aArg = $this->password($aArg);

		$aArg['aRequest']['sPhoto'] = $this->image($aArg,$this->get($sId)['data'][0]['sPhoto']);

		$this->CI->db->where('users.iUserId = '.$id);
		$this->update($aArg['aRequest']);

		return array('sMessage' => 'Record saved!', 'iUserId' => $id);
	}

	private function password($aRequest = [])
	{
		if(strlen($aRequest['aRequest']['sPassword']) > 0) $aRequest['aRequest']['sPassword'] = md5($aRequest['aRequest']['sPassword']);
		else unset($aRequest['aRequest']['sPassword']);

		return $aRequest;
	}

	private function image($aArg,$sOld = null) {

		if(!isset($aArg['aRequest']['sPhoto']) || $aArg['aRequest']['sPhoto'] == '') return null;
		if($sOld) unlink(PATH_BASE_UPLOADS.'users/'.$sOld);

		$sPhoto = 'users/'.sha1($aArg['aRequest']['sEmail']);
		// save image
		$oImage = base64_decode(preg_replace('/^data:.*;base64,/i','',$aArg['aRequest']['sPhoto']));

		$fOpen = fopen(PATH_BASE_UPLOADS.$sPhoto,'w');
		if($fOpen) {
			file_put_contents(PATH_BASE_UPLOADS.$sPhoto, $oImage);
			fclose($fOpen);

			return $sPhoto;
		}

		return null;
	}
}