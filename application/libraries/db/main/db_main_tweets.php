<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once PATH_BASE_TOOLS.'TwitterOAuth.php';

class Db_main_tweets extends Db_core
{

	public $CI 			= null;

	protected $sModule  = '';
	protected $sTable 	= 'tweets';
	protected $sIndex 	= 'id';

	protected $aResp;

	protected $aColumns = [
		'id_str' 					=> 'tweets.id_str',
		'text' 						=> 'tweets.text',
		'in_reply_to_status_id' 	=> 'tweets.in_reply_to_status_id',

	];
	protected $aFields = [
		'id_str',
		'created_at',
		'text',
		'in_reply_to_status_id'
	];

	protected $keywords = [
		'climate' => [
			'accumulation' , 'accumulations' , 'advisory' , 'advisories' , 'air' , 'air pollution' , 'atmosphere' , 'atmospheric pressure' , 'balmy' , 'breeze' , 'climate' , 'cloudy' , 'cold' , 'degree' , 'degrees' , 'disturbances' , 'downpour' , 'downpours' , 'drizzle' , 'drizzles' , 'drought' , 'El Niño' , 'fog' , 'foggy' , 'freezing rain' , 'freezing weather' , 'global warming' , 'greenhouse effect' , 'greenhouse effects . gust' , 'hail' , 'hail storms' , 'heat' , 'hot' , 'heat wave' , 'heat waves . humid' , 'humidity' , 'landfall' , 'muggy' , 'partly cloudy' , 'rain' , 'rains' , 'rainy' , 'rain showers' , 'sky' , 'smog' , 'smogs' , 'storm' , 'storms' , 'storm surge' , 'storm tracks' , 'subtropical' , 'summer' , 'sunny' , 'sunrise' , 'sunset' , 'supercell' , 'surge' , 'temperature' , 'temperatures' , 'thunder' , 'thunders' , 'thunderstorm' , 'thunderstorms' , 'tropical' , 'twilight' , 'tropical depression' , 'tropical depressions' , 'tropical disturbance' , 'tropical disturbances' , 'tropical storm' , 'tropical storms' , 'typhoon' , 'typhoons' , 'typhoons' , 'upwind' , 'warm' , 'water' , 'wave' , 'waves' , 'weather' , 'weather map' , 'whirlwind' , 'whirlwinds' , 'wind' , 'zone' , 'zones'
		],
		'government' => [
			'government', 'governance', 'policies', 'policy', 'fund', 'funds', 'law enforcer', 'law enforcers', 'law enforcements', 'law enforcement', 'permit', 'permits', 'legalities', 'corruption', 'corrupt', 'corrupts', 'official', 'officials', 'strategy', 'tactics. strategies', 'tactic', 'authority', 'authorities', 'regime', 'governing', 'politics', 'polical', 'govt', 'administration', 'sector', 'sectors', 'mayor', 'councilors', 'officials', 'ministers', 'treasury', 'public', 'private', 'leadership', 'communities', 'departments', 'rules', 'NGO', 'NGOs. LGU', 'LGUs', 'voters', 'management', 'offices', 'office', 'democracy', 'democratic', 'politicians', 'lawmakers', 'government units', 'government unit', 'administrative unit', 'administrative units', 'police', 'barangay hall', 'city hall', 'political unit', 'judges', 'public law', 'mandate', 'public assistance', 'policymakers', 'government officials', 'public officials', 'government official', 'public official', 'fundamental laws', 'government strategy', 'government strategies', 'government tactic', 'government tactics', 'law', 'ordinance', 'ordinances', 'agency', 'agencies'
		],
		'culture' => [
			'Ability', 'Abilities', 'acknowledge', 'acknowledges', 'address', 'addresses', 'aesthetics', 'aestheticism . Analysis', 'Ancient', 'Anthropology', 'Anthropological', 'Archeology', 'Archeological', 'Art', 'Arts', 'Artistic', 'Assistance', 'Association', 'Asscociations', 'attitude', 'Authentic', 'Aunthenticity', 'accountable', 'accountability', 'affection', 'affectionate', 'agreable', 'agreeableness', 'amiable', 'amiability', 'attitude', 'attitudes', 'benevolence', 'benevolent', 'brave', 'bravery', 'care', 'cared', 'careful', 'caring', 'capacity', 'capable', 'capability', 'caution', 'cautious', 'centuries', 'charity', 'civilization', 'civilized', 'cleanliness', 'compassion', 'compassionate . concern', 'confidence', 'confident', 'consideration', 'considerate', 'contentment', 'cooperation', 'cooperative', 'courage', 'courageous', 'courtesy', 'courteous', 'creative', 'creativity', 'cultivation', 'cultivate', 'culture . cultural', 'customs', 'delicacy', 'delicacies', 'determination', 'devotion', 'devoted', 'dignity', 'diligence', 'diligent', 'discipline', 'disciplined', 'discretion', 'discrimination', 'discriminate', 'discriminated', 'discriminates', 'dress', 'duty', 'duties', 'earnest', 'earnestly', 'elegance', 'elegant', 'elegantly', 'enthusiasm', 'enthusiastic', 'ethical', 'ethics', 'excellence', 'excellent', 'faithful', 'faithfulness', 'fashion', 'fashionistas', 'finish', 'flexible', 'flexibility', 'friendliness', 'focus', 'focuses', 'focused', 'forgive', 'forgives. forgiveness', 'fortitude', 'friendly', 'friendliness', 'frugality', 'frugal', 'generous', 'generosity', 'gentle', 'gentleness', 'good', 'goodwill', 'grace', 'gracious', 'graciousness', 'gratitude', 'helpful', 'helpfulness', 'heritage', 'historical', 'honest', 'honesty', 'honor', 'honorable', 'hope', 'hopeful', 'hopefulness', 'humane', 'humanity', 'humble', 'humbleness', 'humility', 'humor', 'humorous', 'idealism', 'impartiality', 'innocence', 'integrity', 'intelligence', 'intelligent', 'joy', 'joyful', 'joyfulness', 'justice', 'kind', 'kindness', 'knowledgeable', 'learning', 'learnings', 'leniency', 'lenient', 'love', 'lovable', 'loyalty', 'loyal', 'magnanimity', 'manners', 'mentality', 'mercy', 'merciless', 'mindset', 'moderation', 'modesty', 'moral', 'morals', 'morality', 'nice', 'nicest', 'nobility', 'noble', 'obedience', 'obedient', 'openness', 'orderliness', 'patience', 'patient', 'peacefulness', 'perception', 'perceptions', 'perseverance', 'persistence', 'persistent', 'philosophy', 'polish', 'polished', 'polite', 'politeness', 'practice', 'practices', 'prehistoric', 'probity', 'proficient', 'proficiency', 'propriety', 'prudence', 'prudent', 'purity', 'purposefulness', 'questioning', 'quiet', 'refinement', 'refined', 'reliability', 'reliable', 'religion . religious', 'reputation', 'reputable', 'respect', 'respectfulness', 'respectful', 'resilient', 'resilience', 'resourcefulness', 'resourceful', 'respect', 'respectable', 'responsibility', 'responsible', 'restraint', 'restraints', 'reverence', 'righteousness', 'righteous', 'savoir-faire', 'sensitivity', 'sensitive', 'self-discipline', 'self-disciplined', 'selflessness', 'selfless', 'simple', 'simplicity', 'sincerity', 'sincere ', 'spirit', 'spirituality', 'spontaneity', 'spontaneous', 'steadfast', 'steadfastness', 'strength', 'strengths', 'structure', 'structural', 'structures', 'sympathy', 'sympathetic', 'tact', 'tactfulness', 'tender', 'tenderness', 'thrift', 'thrifty', 'tolerance', 'tolerate', 'toughness', 'tough', 'training', 'trained. well-trained', 'tranquility', 'tranquil', 'trust', 'trustworthiness', 'truthful', 'truthfulness', 'understanding', 'unity', 'united', 'upstanding', 'urban . urbanity', 'virtue', 'virtues', 'virtuous', 'vitality', 'wholesome', 'wholesomeness', 'wisdom', 'wonder', 'wonderment', 'worthiness', 'worthy', 'zeal', 'zealous'
		],
		'economy' => [
		    'economy', 'economic', 'recession', 'economical', 'econimically', 'market', 'financial', 'finances', 'business', 'businesses', 'industry', 'commerce', 'economics', 'profitability', 'profit', 'profits', 'finance', 'enterprise', 'savings', 'assets', 'Asset', 'trade', 'trading', 'economic growth', 'sales', 'cost', 'costs', 'cost-effectiveness', 'cost-efficiency', 'socio-economic', 'upturn', 'downturn', 'inflation', 'employment', 'unemployment', 'employed', 'unemployed', 'prosperity', 'prosperous', 'crisis', 'disposable income', 'income', 'incomes', 'tourism', 'deflation', 'capital', 'capitalism', 'stagflation', 'agriculture', 'macroeconomics', 'currency', 'agriculture', 'mining', 'farming', 'command economy', 'supply', 'demand', 'industries', 'jobs', 'part-time jobs', 'prices', 'price', 'industrialization', 'deindustrialization', 'stagnant', 'stagnation', 'monetary policy', 'infrastructure', 'infrastractures', 'dollar', 'peso', 'euro', 'consumer', 'consumers', 'producer', 'producers', 'bankrupt', 'bankruptcies', 'banks', 'exporters', 'importers', 'exporter', 'importer', 'disinflation', 'hyperinflation', 'entrepreneur', 'entrepreneurs', 'entrepreneurship', 'global', 'stockmarket', 'stock exchange', 'workforce', 'economic process', 'black market', 'monetary', 'monetary funds', 'products', 'produce. money', 'goods', 'marketplace', 'marketplaces', 'microeconomy', 'free market', 'gross', 'investment', 'investments', 'payments', 'payment', 'joblessness', 'stock', 'stocks'
		],
		'education' => [
			'school', 'schools', 'teaching', 'training', 'trainings', 'breeding', 'education', 'educational . curriculum', 'curriculums', 'schooling', 'Literate . illiterate', 'literacy', 'curricula', 'educators', 'teacher', 'teachers', 'academic', 'academics', 'tuition', 'educator', 'diploma', 'diplomas', 'education', 'classrooms', 'classroom', 'educated', 'students', 'student', 'outreach', 'scholarships', 'scholarship', 'civilized', 'tertiary', 'secondary', 'primary', 'college', 'colleges', 'prep', 'kindergarten', 'university', 'universities'
		],
		'location' => [
		    'place', 'places', 'city', 'landmark', 'landmarks', 'park . parks', 'mall', 'malls', 'community park', 'schools', 'subdivision . school', 'subdivisions', 'home', 'homes', 'resort', 'resorts', 'restaurants . restaurant', 'bars', 'eatery', 'churches', 'mansion', 'mansions', 'establishments . establishment', 'food park', 'food parks', 'hospital', 'hospitals', 'water district', 'road', 'roads', 'eateries', 'theater', 'theaters', 'street', 'streets', 'site', 'sites', 'heritage site', 'heritage sites', 'paradise', 'environment', 'hotel', 'hotels', 'motel', 'motels', 'location', 'locations', 'lodge', 'spot', 'position', 'destination', 'venue', 'area', 'facility', 'venues', 'geography', 'premises', 'station', 'stationed', 'town', 'residence', 'residences', 'residential areas', 'spaces', 'rentals', 'rental', 'address', 'addresses', 'accomodation', 'accomodations', 'home', 'village', 'villages', 'distance', 'environment', 'surrounding', 'scenery', 'sceneries'
		],
		'population' => [
		    'population', 'number of people', 'populating', 'populace', 'inhabitants', 'populous', 'census', 'demography', 'demographic', 'demographics', 'caseload', 'workforce', 'communities', 'community', 'citizens', 'citizen', 'residents', 'citizenry', 'society', 'socities', 'populated', 'cohort', 'community', 'inhabitant', 'species', 'proportion', 'living', 'dwellers', 'density', 'habitat', 'housing', 'herd', 'percentage of the population', 'rate of the population', 'population rate', 'clientele', 'audiences', 'townspeople', 'townsmen', 'villagers', 'citizen', 'locals', 'offenders', 'laborers', 'laborer', 'mob', 'crowded'
		],
		'resources' => [
		    'minerals. mineral', 'energy', 'mountains', 'forests', 'forest', 'fish', 'fishes', 'animals', 'meat', 'meats', 'trees', 'wood', 'woods. water', 'waters', 'lake', 'lakes', 'seafoods', 'poultry', 'pastures', 'land', 'lands', 'crops', 'fruits', 'fruit trees', 'drinking water', 'oil', 'gasoline', 'fuel', 'medicine', 'pulp', 'wildlife', 'rubber', 'cotton', 'rocks', 'agriculture', 'agricultural', 'farming', 'fisheries', 'cattle', 'plants', 'planting', 'raw materials', 'organics', 'inorganics', 'renewable', 'non-renewable', 'fossil fuels', 'vegetation . metallic', 'non-metallic', 'ecosystem', 'plantain', 'plantains', 'vegetables', 'vegetable'
		]
	];

	function __construct(){
		$this->CI =& get_instance();

		$this->CI->load->library('tools/tools_opinion_mining');
		$this->CI->tools_opinion_mining->addToIndex(PATH_BASE_APP.'libraries/tools/opinions/replies.pos','pos');
		$this->CI->tools_opinion_mining->addToIndex(PATH_BASE_APP.'libraries/tools/opinions/replies.neg','neg');

		$this->aResp['tweets'] = [];
	}

	public function get($sId = null ,$aArg = array())
	{
		$this->CI->db->select($this->select($aArg, array_merge(
			$this->aColumns,
			array(
				'created_at'				=> 'tweets.created_at'
			)
		)));

		$this->CI->db->from('tweets');

		$this->limit($aArg);
		$this->CI->db->order_by('tweets.created_at ASC');
		$this->search($aArg);

		$aData 		= $this->CI->db->get();
		$aReturn 	= $aData->result_array();
		$data 		= $this->getResult($aReturn,$aArg);

		$pos = 0;
		$neg = 0;

		$parameters = [
			'POSITIVE' => [
				'climate' 		=> 0,
				'government' 	=> 0,
				'culture' 		=> 0,
				'economy' 		=> 0,
				'education' 	=> 0,
				'location' 		=> 0,
				'population' 	=> 0,
				'resources' 	=> 0
			],
			'NEGATIVE' => [
				'climate' 		=> 0,
				'government' 	=> 0,
				'culture' 		=> 0,
				'economy' 		=> 0,
				'education' 	=> 0,
				'location' 		=> 0,
				'population' 	=> 0,
				'resources' 	=> 0
			],
		];

		$paramsOnly = isset($_REQUEST['paramsOnly']);
		$params = isset($_REQUEST['key']) ? $_REQUEST['key'] : null;
		$sentiment = isset($_REQUEST['sentiment']) ? $_REQUEST['sentiment'] : null;

		/** Filters Topic and replies */
		foreach($data as $index => $row)
		{
			// sentiment analysis
			$willInclude = true;
			$analysis = $this->analyseSentiment($row['text']);

			if($analysis == 'pos') {
				$analysis = 'POSITIVE';
				$pos++;
			} else if($analysis == 'neg'){
				$analysis = 'NEGATIVE';
				$neg++;
			}

			// parameters analylis
			foreach( $this->keywords as $key => $arr) {
				$valid = $this->validateParameters($row['text'],$arr);

				$parameters[$analysis][$key]+= $valid;

				if( $params && $params == $key && !$valid ) {
					$willInclude = false;
				}
			}

			if(
				$willInclude &&
				(($sentiment && $analysis == $sentiment) || !$sentiment) &&
				!$paramsOnly
			) {
				array_push($this->aResp['tweets'],array_merge($row,[ 'analysis' => $analysis ]));
			}

		}

		return array_merge($this->aResp, [
			'total'	=> $neg+$pos,
			'neg' => $neg,
			'neg_perc' => number_format(($neg/($neg+$pos))*100,2),
			'pos' => $pos,
			'pos_perc' => number_format(($pos/($neg+$pos))*100,2),
			'params' => $parameters
		]);
	}

	private function analyseSentiment( $tweetText )
	{
		return $this->CI->tools_opinion_mining->classify($tweetText);
	}

	// returns counts of keywords present in the tweet
	private function validateParameters($tweet, $keywords) {
		return count( array_filter( $keywords , create_function('$e','return strstr("'.$tweet.'", $e);'))) > 0;
	}

	public function add($aArg)
	{
		$aId = $this->insert($aArg['aRequest']);
		return array('sMessage' => 'Record saved!', 'id' => $aId['id']);
	}

	public function edit($aArg = array())
	{

	}
}
