<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once PATH_BASE_TOOLS.'TwitterOAuth.php';

class Db_main_sync extends Db_core
{

	public $CI 			= null;

	protected $sModule  = '';
	protected $sTable 	= 'tweets';
	protected $sIndex 	= 'iTweetId';

	private $consumer 				= null;
	private $consumer_secret 		= null;
	private $access_token 			= null;
	private $access_token_secret 	= null;

	private $oTwitter 				= null;

	function __construct(){
		$this->CI =& get_instance();

		$this->CI->load->library('db/main/db_main_tweets');

		$this->oTwitter = new TwitterOAuth( $this->CI->config->item('twitter_consumer_token'), $this->CI->config->item('twitter_consumer_secret'), $this->CI->config->item('twitter_access_token'), $this->CI->config->item('twitter_access_secret'));
		// $this->oTwitter = new TwitterAPIExchange(
		// 	array(
		// 	    'oauth_access_token' => $this->CI->config->item('twitter_access_token'),
		// 	    'oauth_access_token_secret' => $this->CI->config->item('twitter_access_secret'),
		// 	    'consumer_key' => $this->CI->config->item('twitter_consumer_token'),
		// 	    'consumer_secret' => $this->CI->config->item('twitter_consumer_secret')
		// 	)
		// );
	}

	protected function getTweetsByHashtag($sHashtag, $since_id = null)
	{
		return $this->oTwitter->get('search/tweets',array('q' => $sHashtag,'count' => 100));
	}

	protected function getTweetsById($sId)
	{
		return $this->oTwitter->get('statuses/show/'.$sId, array('include_replies' => 'true', 'include_my_retweet' => true));
	}

	protected function getRetweetsById($sId)
	{
		return $this->oTwitter->get('statuses/retweets/'.$sId, array('include_replies' => 'true', 'count' => 200));
	}

	public function get($sId = null ,$aArg = array())
	{
		// $url    = 'https://api.twitter.com/1.1/search/tweets.json';
		// $getfield = '?q=%23whatsinlipa';
		// $requestMethod = 'GET';
		//
		//
		// $data = $this->oTwitter->request($url, $requestMethod, $getfield);
		// $data = (array)@json_decode($data, true);
		//
		// print_r($data['statuses']);
		// exit;

		$this->CI->db->select_max('id_str');
		$result = $this->CI->db->get('tweets')->result_array();

		$data = $this->getTweetsByHashtag('WhatsInLipa',($result ? $result[0]['id_str'] : null))->statuses;
		$arr_length = sizeof($data);

		for($i = $arr_length; $i > 0; $i--)
		{
			$aRow = $data[$i-1];

			$this->CI->db_main_tweets->add(['aRequest' => [
				'created_at' 					=> $aRow->created_at,
				'id_str' 	 					=> $aRow->id_str,
				'text' 	 						=> $aRow->text
			]]);
		}

		exit();

	}

	private function reverse_sorting($arr)
	{
		$aTemp = [];
		$arr_length = sizeof($arr);

		for($i = $arr_length; $i > 0; $i--)
		{
			array_push($aTemp, $arr[$i-1]);
		}

		return $aTemp;
	}
	public function add($aArg)
	{

	}

	public function edit($aArg = array())
	{

	}
}
