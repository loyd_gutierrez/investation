<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_main_roles extends Db_core
{
	
	public $CI 			= null;

	protected $sModule  = '';
	protected $sTable 	= 'roles';
	protected $sIndex 	= 'iRoleId';

	protected $aColumns = array(
		'sRole'					=> 'roles.sRole',
		'bActive'				=> 'roles.bActive'
	);

	protected $aFields = array(

		'sRole',
		'bActive'

	);

	function __construct(){
		$this->CI =& get_instance();
	}

	public function get($sId = null ,$aArg = array())
	{
		$this->CI->db->select($this->select($aArg, array_merge(
			$this->aColumns,
			array(
				'bACtive'				=> 'roles.bActive',
				'iRoleId'				=> 'roles.iRoleId',
			)
		)));

		$this->CI->db->from($this->sTable);


		$this->where($aArg, 'iRoleId','roles');
		
		$this->order($aArg);
		$this->limit($aArg);
		
		$this->search($aArg);

		$aData 		= $this->CI->db->get();
		$aReturn 	= $aData->result_array();

		return array_merge(
			$this->getFoundRows(),
			array('data' => $this->getResult($aReturn,$aArg))
		);
	}

	public function add($aArg)
	{
		
	}

	public function edit($aArg = array())
	{
		
	}
}