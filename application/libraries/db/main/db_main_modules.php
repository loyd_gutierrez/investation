<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Db_main_modules extends Db_core
{
	
	public $CI 			= null;

	function __construct(){
		$this->CI =& get_instance();
	}

	public function get($sId = null,$aArg = array())
	{
		$aUserInfo = $this->CI->session->userdata('aInfo');

		switch($aUserInfo['sRole']){
			case "Administrator":
				return array(
						'iUserId' => $aUserInfo['iUserId'],
						'sRole' => $aUserInfo['sRole'],
						'sFullName' => $aUserInfo['sFullName'],
						'sPhoto' 	=> $aUserInfo['sPhoto'],
						'sLandingPage' => 'Vessels',
						'aModules' => array(
							array( 'sName' => 'Vessels', 'sTitle' => 'Vessels','bNavigation'=>true,'bList' => true, 'bForm' => true, 'sIcon' => 'fa-ship', 'aPermissions' => array('menu','add','edit','delete','pdf') ),
							
							array( 'sName' => 'Users', 'sTitle' => 'Users','bNavigation'=>true,'bList' => true, 'bForm' => true, 'sIcon' => 'fa-users', 'aPermissions' => array('menu','add','edit','delete') ),

							array( 'sName' => 'Managements', 'sTitle' => 'Management','bNavigation'=>true,'bList' => false, 'bForm' => false, 'sIcon' => 'fa-gears', 'aPermissions' => array('menu'), 'aChildren' => array(
									array( 'sName' => 'VesselCategories', 'sTitle' => 'Vessel Categories','bNavigation'=>true,'bList' => true, 'bForm' => true, 'sIcon' => 'fa-sitemap', 'aPermissions' => array('menu','add','edit','delete') ),
									array( 'sName' => 'VesselTypes', 'sTitle' => 'Vessel Types','bNavigation'=>true,'bList' => true, 'bForm' => true, 'sIcon' => 'fa-sitemap', 'aPermissions' => array('menu','add','edit','delete') ),
									array( 'sName' => 'RegistrationTypes', 'sTitle' => 'RegistrationTypes','bNavigation'=>true,'bList' => true, 'bForm' => true, 'sIcon' => 'fa-sitemap', 'aPermissions' => array('menu','add','edit','delete') ),
								)
							),
						)
				);
			break;

			case "Personnel":

				return array(
						'iUserId' => $aUserInfo['iUserId'],
						'sRole' => $aUserInfo['sRole'],
						'sFullName' => $aUserInfo['sFullName'],
						'sPhoto' 	=> $aUserInfo['sPhoto'],
						'sLandingPage' => 'Vessels',
						'aModules' => array(
							array( 'sName' => 'Vessels', 'sTitle' => 'Vessels','bNavigation'=>true,'bList' => true, 'bForm' => true, 'sIcon' => 'fa-ship', 'aPermissions' => array('menu','add','edit','delete','pdf') ),
							array( 'sName' => 'Users', 'sTitle' => 'Users','bNavigation'=>false,'bList' => false, 'bForm' => true, 'sIcon' => 'fa-users', 'aPermissions' => array('add','edit','delete') ),
						)
				);
			break;

			default:
				return array('iErr' => 1, 'sMessage' => 'Permission Denied! Role not found');
			break;
		}

	}
}