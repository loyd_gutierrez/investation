<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hooks_pre_system
{
	public function __construct()
	{
	}

	public function init()
	{
		$this->constants();
		$this->session();
	}

	private function constants()
	{
		define('HTTP_HOST', $_SERVER['HTTP_HOST']);
		define('SUB_DOMAIN', (
			HTTP_HOST == 'eesn' ?
			'eesn' :
			substr(HTTP_HOST, 0, strpos(HTTP_HOST, '.'))
		));
		define('SITE_URL', (
			HTTP_HOST == 'localhost' ?
			'http://localhost/lipacab/' :
			'http://'.HTTP_HOST.'/lipacab/'
		)); 

		$_SERVER['HTTP_HOST'] == 'localhost' ?
			define('PATH_BASE_FOLDER',				'assets/files/') :
			define('PATH_BASE_FOLDER',				'assets/files/');
		
		define('PATH_BASE_APP',					FCPATH.APPPATH);

		define('PATH_BASE_UPLOADS',				PATH_BASE_FOLDER.'uploads/');

		define('PATH_BASE_TOOLS',				PATH_BASE_APP.'libraries/db/tools/');		

		define('PATH_LIBRARIES_DB',				PATH_BASE_APP.'libraries/db/temp/');
		define('PATH_VIEWS_FORMS',				PATH_BASE_APP.'views/modals/');
		define('PATH_VIEWS_LIST',				PATH_BASE_APP.'views/dashboards/');
		define('PATH_ASSETS_JS',				FCPATH.'assets/js/');
	}

	private function session()
	{
		session_save_path('assets/sessions');
		session_start();

	}
}


