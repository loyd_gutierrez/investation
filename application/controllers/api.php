<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @name API controller
 * @author      JL Gutierrez <lloydgutierez24@gmail.com>
 * @version		1.0.0
 */

class Api extends CI_Controller
{
	private $aError_codes = [
		'401' 	=> 'Access Denied',
		'401.1' => 'Login Error',
		'403' 	=> 'Forbidden Access',
		'404' 	=> 'Request not found'

	];

	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods','GET,PUT,POST,DELETE,PATCH,OPTIONS');
		header('conten-type:json');

		parent::__construct();

		$this->load->model('rest');
		$this->load->model('dt');
			
		$this->processRequest($this->getRequestType());
	}

	/**
	 * Get function
	 */
	private function processRequest($sRequestType = null)
	{
		if(!$sRequestType) return $this->response($this->error(404));

		$sMethod = strtolower($this->uri->segment(2));
		$sId = $this->uri->segment(3);
		$aData = $this->parse(file_get_contents("php://input"));

		// if($sMethod !== 'auth' && !$this->validateSession()) return $this->response($this->error(401));
		if($sMethod == 'tables') return $this->response($this->tables());

		if($sMethod == 'certificates') return $this->response($this->certificates());

		if(method_exists($this->rest,$sRequestType)) {
			return $this->response($this->rest->$sRequestType($sMethod,$sId,$aData));
		}

		return $this->response($this->error(404));
	}

	private function certificates()
	{
		$aRecord = [];

		$sMethod = 'db_main_'.strtolower($this->uri->segment(3));
		$sId = $this->uri->segment(4);

		$this->load->model('certificates');
		$this->load->library('db/main/'.$sMethod);
		$this->load->library('db/main/db_main_users');

		$aRecord = $this->$sMethod->get($sId)['data'][0];
		return $this->certificates->print_certificate(strtolower($this->uri->segment(3)),$aRecord);
	}

	private function tables()
	{
		$sMethod = $this->uri->segment(3);
		$aData = $this->parse(array_keys($_REQUEST)[0]);

		return $this->dt->get($sMethod,$aData);
	}

	/**
	 * Get's request type
	 * @return string
	 */
	private function getRequestType()
	{
		$sMethod = strtolower($this->input->server('REQUEST_METHOD'));
		if(in_array($sMethod,['get','put','post','delete'])){
			return $sMethod;
		}

		return null;
	}

	private function validateSession()
	{
		return $this->tools_sessions->logged();
	}

	/**
	 * Unparse the raw data
	 * @return JSON
	 */
	private function parse($sData)
	{
		if(!$sData) return;

		return json_decode(base64_decode($sData,TRUE),TRUE);
	}

	private function error($iCode)
	{
		return [
			'iError' => $iCode,
			'sMessage' => $this->aError_codes[ $iCode ]
		];
	}

	private function response($aResp)
	{
		if(ENVIRONMENT == 'development') {
			header('content-type:json');
			print_r(json_encode($aResp));
			exit();
		}
		
		print_r( base64_encode(json_encode($aResp)) );
		exit();
	}
}