webpackJsonp([1,5],{

/***/ 266:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 266;


/***/ }),

/***/ 267:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(274);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(276);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(279);




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 275:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_http_service__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_windowreference_service__ = __webpack_require__(95);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AppComponent = (function () {
    function AppComponent(titleService) {
        this.titleService = titleService;
        this.titleService.setTitle('InveStation');
    }
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(390),
        styles: [__webpack_require__(381)],
        providers: [
            __WEBPACK_IMPORTED_MODULE_1__services_http_service__["a" /* HttpService */],
            __WEBPACK_IMPORTED_MODULE_3__services_windowreference_service__["a" /* WindowReferenceService */]
        ]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["b" /* Title */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["b" /* Title */]) === "function" && _a || Object])
], AppComponent);

var _a;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 276:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(273);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_routing__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(275);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__home_home_component__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__tweets_tweets_component__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__spinners_spinners_component__ = __webpack_require__(278);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_charts__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_ng2_charts__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_6__home_home_component__["a" /* HomeComponent */],
            __WEBPACK_IMPORTED_MODULE_7__tweets_tweets_component__["a" /* TweetsComponent */],
            __WEBPACK_IMPORTED_MODULE_8__spinners_spinners_component__["a" /* SpinnersComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_9_ng2_charts__["ChartsModule"],
            __WEBPACK_IMPORTED_MODULE_4__app_routing__["a" /* routing */]
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["b" /* Title */]],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 277:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__home_home_component__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tweets_tweets_component__ = __webpack_require__(96);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routing; });



var appRoutes = [
    {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_1__home_home_component__["a" /* HomeComponent */]
    },
    {
        path: 'tweets',
        component: __WEBPACK_IMPORTED_MODULE_2__tweets_tweets_component__["a" /* TweetsComponent */]
    },
    {
        path: 'tweets/:params/:sentiment',
        component: __WEBPACK_IMPORTED_MODULE_2__tweets_tweets_component__["a" /* TweetsComponent */]
    }
];
var routing = __WEBPACK_IMPORTED_MODULE_0__angular_router__["a" /* RouterModule */].forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map

/***/ }),

/***/ 278:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(12);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpinnersComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SpinnersComponent = (function () {
    function SpinnersComponent() {
    }
    SpinnersComponent.prototype.ngOnInit = function () {
    };
    return SpinnersComponent;
}());
SpinnersComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'spinner',
        template: __webpack_require__(392),
        styles: [__webpack_require__(383)]
    }),
    __metadata("design:paramtypes", [])
], SpinnersComponent);

//# sourceMappingURL=spinners.component.js.map

/***/ }),

/***/ 279:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 381:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(32)(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 382:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(32)(false);
// imports


// module
exports.push([module.i, ".text-centered {\r\n\ttext-align: center;\r\n}\r\n\r\nheader .navbar{\r\n\tmargin-bottom: 0px;\r\n\tbackground: #01579b;\r\n\tborder: 0px;\r\n\tborder-radius: 0px;\r\n}\r\nheader .navbar-nav{\r\n}\r\n\r\na.navbar-brand img{\r\n\theight: 65px;\r\n\tpadding-left: 30px;\r\n}\n\n@media(max-width:767px) {\r\n\ta.navbar-brand {\r\n\t\tpadding: 1px;\r\n\t}\r\n\r\n\ta.navbar-brand img{\r\n\t\theight: 48px;\r\n\t\tpadding-left: 10px;\r\n\t}\r\n}\r\n\r\n.navbar-default .navbar-nav > li > a{\r\n\tcolor: #FFF;\r\n\ttransition: .2s ease-out;\r\n\tborder-bottom: 2px solid transparent;\r\n\tpadding:30px;\r\n\r\n}\r\n.navbar-default .navbar-nav>li>a:focus,\r\n.navbar-default .navbar-nav>li>a:hover,\r\n.navbar-default .navbar-nav>.active>a,\r\n.navbar-default .navbar-nav>.active>a:focus,\r\n.navbar-default .navbar-nav>.active>a:hover{\r\n\tcolor:#FFF;\r\n\tborder-bottom: 2px solid #FFF;\r\n}\r\ndiv.header{\r\n\tbackground: url('assets/images/jumbotron.jpg') no-repeat fixed center;\r\n\tbackground-size: cover;\r\n\theight: 90%;\r\n\tpadding-top: 10%;\r\n}\r\n.parallax {\r\n\theight: 100%;\r\n\tbackground-attachment: fixed;\r\n\tbackground-position: center;\r\n\tbackground-repeat: no-repeat;\r\n\tbackground-size: cover;\r\n}\r\n.div-title{\r\n\ttext-align: center;\r\n\tcolor: #FFF;\r\n}\r\n.div-title h1{\r\n\tfont-size: 50px;\r\n\ttext-shadow: 2px 1px #023a67;\r\n}\r\n.div-title h2{\r\n\ttext-shadow: 2px 1px #023a67;\r\n}\r\n\r\nbutton.header-button {\r\n\tbackground-color: transparent;\r\n\tcolor: #FFF;\r\n\tborder:1px solid #FFF;\r\n\tpadding:1em 4em;\r\n\r\n\ttext-decoration: none;\r\n\tbox-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);\r\n\ttransition: .3s ease-out;\r\n\tfont-weight: bold;\r\n\ttext-transform: uppercase;\r\n\tword-spacing: .5px;\r\n}\r\n\r\nbutton.header-button:focus,\r\nbutton.header-button:hover,\r\nbutton.header-button:active{\r\n\r\n\tcolor: #FFF;\r\n\tpadding:1em 5em;\r\n\tbackground: #3d4747;\r\n\topacity: 0.7;\r\n}\r\n\r\n.btn-follow{\r\n\ttext-decoration: none;\r\n\tcolor: #FFF;\r\n\tborder: none;\r\n\tbackground-color: rgba(0,145,234,0.9);\r\n\r\n\tbox-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 1px 5px 0 rgba(0,0,0,0.12), 0 3px 1px -2px rgba(0,0,0,0.2);\r\n\ttransition: .3s ease-out;\r\n\tfont-weight: bold;\r\n\ttext-transform: uppercase;\r\n\tword-spacing: .5px;\r\n}\r\n\r\na.btn-follow:focus,\r\na.btn-follow:hover,\r\na.btn-follow:active{\r\n\tbackground-color: transparent;\r\n\tcolor: #FFF;\r\n\tborder:1px solid #FFF;\r\n\tpadding:1em 5em;\r\n}\r\n.btn-follow{\r\n\tpadding:1em;\r\n}\r\na.btn-follow:focus,\r\na.btn-follow:hover,\r\na.btn-follow:active{\r\n\tbackground-color: transparent;\r\n\tcolor: rgba(0,145,234,1);\r\n\tborder:1px solid rgba(0,145,234,1);\r\n\tpadding:1em 2em;\r\n}\r\ndiv.col-centered{\r\n\tfloat: none;\r\n\tmargin: 0 auto;\r\n}\r\nh3.section-header{\r\n\tfont-weight: bolder;\r\n\tcolor: #083b66;\r\n\tfont-size:32px;\r\n\tmargin-bottom: 20px;\r\n}\r\n.section-body{\r\n\tpadding: 50px 30px 70px 30px;\r\n}\r\nsection{\r\n\tmin-height: 50%;\r\n}\r\nsection h3{\r\n\ttext-align:center;\r\n}\r\nsection p{\r\n\ttext-align: justify;\r\n}\r\nsection#how-it-works{\r\n\tbackground: #01579b url('assets/images/hiw-background.png') no-repeat fixed 10%;\r\n\tbackground-size: cover;\r\n\tcolor: #fff;\r\n}\r\nsection#how-it-works h3.section-header{\r\n\tcolor: #FFF;\r\n}\r\nsection#parameters {\r\n\tpadding-bottom: 150px;\r\n}\r\n.charts {\r\n\tmargin:15px;\r\n}\r\n\r\nsection#meet-the-team{\r\n\tbackground: #eceff1 url('assets/images/team-background.jpg') no-repeat fixed 50% 50%;\r\n\tbackground-size: cover;\r\n}\r\nsection#contact{\r\n\tbackground-color: #424242;\r\n\tcolor:#FFF;\r\n}\r\nsection#contact h3.section-header{\r\n\tcolor:#FFF;\r\n}\r\n\r\nsection#contact div.contact-info,\r\nsection#contact div.contact-info p{\r\n\ttext-align: center !important;\r\n}\r\n\r\ndiv.contact-info i{\r\n\tcolor: #bdbdbd;\r\n}\r\n\r\ndiv.team-member{\r\n\ttext-align: center;\r\n\tpadding:20px 10px 50px 10px;\r\n}\r\nh4.member-name{\r\n\tfont-weight: bold;\r\n}\r\ndiv.team-member span{\r\n\tcolor: #01579b;\r\n}\r\n\r\ndiv.ProfileAvatar {\r\n\tbackground: #fff;\r\n\tborder: 5px solid #fff;\r\n\tborder-radius: 50% !important;\r\n\theight: 200px;\r\n\twidth: 200px;\r\n\toverflow: hidden;\r\n\tmargin: auto;\r\n}\r\n\r\ndiv.team-member img{\r\n\twidth:100%;\r\n}\r\ndiv.team-member p{\r\n\ttext-align: center;\r\n\tmargin-top: 10px;\r\n}\r\nfooter{\r\n\tbackground-color: #2e2e2e;\r\n\theight: 50px;\r\n}\r\nfooter p.copyright{\r\n\tpadding:15px;\r\n\tcolor:#424242;\r\n\tmargin: 0px;\r\n}\r\n\r\n@media(min-width:768px){\r\n\t.navbar>.container .navbar-brand, .navbar>.container-fluid .navbar-brand{\r\n\t\t/*margin-top: -30px;*/\r\n\t}\r\n\tdiv.header{\r\n\t\tpadding-top:12%;\r\n\t}\r\n\timg.hidelogo{\r\n\t\tdisplay: none;\r\n\t}\r\n}\r\n@media(max-width: 767px){\r\n\timg.hidelogo{\r\n\t\tdisplay: block;\r\n\t\theight: 120px;\r\n\t\tmargin: 0 auto;\r\n\t}\r\n}\r\n\r\ndiv.H100 {\r\n\tmin-height:100px;\r\n}\r\n\r\n.legend {\r\n\tdisplay:inline-block;\r\n\tmargin: 2px 5px;\r\n\tline-height: 15px;\r\n}\r\n\r\n.legend-color {\r\n\twidth:30px;\r\n\theight:15px;\r\n\tborder:solid 1px;\r\n\tmargin: 0px 2px;\r\n}\r\n\r\n.legend div {\r\n\tdisplay: inline-block;\r\n\tfloat:left;\r\n}\r\n\r\n.legend .legend-education, .legend .legend-negative {\r\n\tbackground: #ffa2b5;\r\n}\r\n.legend .legend-economy, .legend .legend-positive {\r\n\tbackground: #8bc7f3;\r\n}\r\n.legend .legend-resources {\r\n\tbackground: #ffe29d;\r\n}\r\n.legend .legend-climate {\r\n\tbackground: #f1f2f4;\r\n}\r\n.legend .legend-location {\r\n\tbackground: #98dad9;\r\n}\r\n.legend .legend-government {\r\n\tbackground: #c1d6e1;\r\n}\r\n.legend .legend-culture {\r\n\tbackground: #eaeaea;\r\n}\r\n.legend .legend-population {\r\n\tbackground: #fd9393;\r\n}\r\n\r\ncanvas {\r\n\tmargin-bottom:50px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 383:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(32)(false);
// imports


// module
exports.push([module.i, ".spinner {\n    /*display: none; /* Hidden by default */\n    position: fixed; /* Stay in place */\n    z-index: 1; /* Sit on top */\n    left: 0;\n    top: 0;\n    width: 100%; /* Full width */\n    height: 100%; /* Full height */\n    overflow: auto; /* Enable scroll if needed */\n    background-color: rgb(0,0,0); /* Fallback color */\n    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */\n}\n\n/* Modal Content/Box */\n.spinner-content {\n    background-color: transparent;\n    position:absolute;\n    top:0;\n    bottom:0;\n    left:0;\n    right:0;\n    margin: auto;\n\n    width:50px;\n    height:50px;\n}\n\n.spinner-content img {\n    width:100%;\n    height:100%;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 384:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(32)(false);
// imports


// module
exports.push([module.i, "header .navbar{\n\tmargin-bottom: 0px;\n\tbackground: #01579b;\n\tborder: 0px;\n\tborder-radius: 0px;\n}\nheader .navbar-nav{\n}\n\na.navbar-brand img{\n\theight: 65px;\n\tpadding-left: 30px;\n}\n\n@media(max-width:767px) {\n\ta.navbar-brand {\n\t\tpadding: 1px;\n\t}\n\n\ta.navbar-brand img{\n\t\theight: 48px;\n\t\tpadding-left: 10px;\n\t}\n}\n\n.navbar-default .navbar-nav > li > a{\n\tcolor: #FFF;\n\ttransition: .2s ease-out;\n\tborder-bottom: 2px solid transparent;\n\tpadding:30px;\n\n}\n\n.navbar-default .navbar-nav > li > a.active,\n.navbar-default .navbar-nav>li>a:focus,\n.navbar-default .navbar-nav>li>a:hover,\n.navbar-default .navbar-nav>.active>a,\n.navbar-default .navbar-nav>.active>a:focus,\n.navbar-default .navbar-nav>.active>a:hover{\n\tcolor:#FFF;\n\tborder-bottom: 2px solid #FFF;\n}\ndiv.header{\n\tbackground: url('assets/images/jumbotron.jpg') no-repeat fixed center;\n\tbackground-size: cover;\n\theight: 90%;\n\tpadding-top: 10%;\n}\n.container {\n    padding-top: 10px;\n    padding-bottom: 10px;\n}\n\n.tweet{\n\tbackground:transparent;\n\tdisplay:inline-block;\n\tfloat:left;\n}\n\n.tweet .panel {\n    border: 0;\n    box-shadow: none;\n}\n.panel .panel-heading {\n    padding-left: 0px;\n}\n\n.panel .panel-title {\n    display: inline-block;\n}\n\n.panel .panel-title.POSITIVE {\n    color: green;\n}\n\n.panel .panel-title.NEGATIVE {\n    color: red;\n}\n\n.panel .panel-heading .loader{\n    background-image: url('assets/images/spinner.gif');\n    background-size: 15px 15px;\n    height: 15px;\n    width: 15px;\n    float: right;\n    position: absolute;\n    margin-left: 130px;\n}\n\n/** Modal */\n/* The Modal (background) */\n.modal {\n    display: none; /* Hidden by default */\n    position: fixed; /* Stay in place */\n    z-index: 1; /* Sit on top */\n    left: 0;\n    top: 0;\n    width: 100%; /* Full width */\n    height: 100%; /* Full height */\n    overflow: auto; /* Enable scroll if needed */\n    background-color: rgb(0,0,0); /* Fallback color */\n    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */\n}\n\n/* Modal Content/Box */\n.modal-content {\n    background-color: #fefefe;\n    margin: 15% auto; /* 15% from the top and centered */\n    padding: 20px;\n    border: 1px solid #888;\n    width: 80%; /* Could be more or less, depending on screen size */\n}\n\n/* The Close Button */\n.close {\n    color: #aaa;\n    float: right;\n    font-size: 28px;\n    font-weight: bold;\n}\n\n.close:hover,\n.close:focus {\n    color: black;\n    text-decoration: none;\n    cursor: pointer;\n}\n\n.analysis_image {\n\theight:60px;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 386:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 112,
	"./af.js": 112,
	"./ar": 119,
	"./ar-dz": 113,
	"./ar-dz.js": 113,
	"./ar-kw": 114,
	"./ar-kw.js": 114,
	"./ar-ly": 115,
	"./ar-ly.js": 115,
	"./ar-ma": 116,
	"./ar-ma.js": 116,
	"./ar-sa": 117,
	"./ar-sa.js": 117,
	"./ar-tn": 118,
	"./ar-tn.js": 118,
	"./ar.js": 119,
	"./az": 120,
	"./az.js": 120,
	"./be": 121,
	"./be.js": 121,
	"./bg": 122,
	"./bg.js": 122,
	"./bn": 123,
	"./bn.js": 123,
	"./bo": 124,
	"./bo.js": 124,
	"./br": 125,
	"./br.js": 125,
	"./bs": 126,
	"./bs.js": 126,
	"./ca": 127,
	"./ca.js": 127,
	"./cs": 128,
	"./cs.js": 128,
	"./cv": 129,
	"./cv.js": 129,
	"./cy": 130,
	"./cy.js": 130,
	"./da": 131,
	"./da.js": 131,
	"./de": 134,
	"./de-at": 132,
	"./de-at.js": 132,
	"./de-ch": 133,
	"./de-ch.js": 133,
	"./de.js": 134,
	"./dv": 135,
	"./dv.js": 135,
	"./el": 136,
	"./el.js": 136,
	"./en-au": 137,
	"./en-au.js": 137,
	"./en-ca": 138,
	"./en-ca.js": 138,
	"./en-gb": 139,
	"./en-gb.js": 139,
	"./en-ie": 140,
	"./en-ie.js": 140,
	"./en-nz": 141,
	"./en-nz.js": 141,
	"./eo": 142,
	"./eo.js": 142,
	"./es": 144,
	"./es-do": 143,
	"./es-do.js": 143,
	"./es.js": 144,
	"./et": 145,
	"./et.js": 145,
	"./eu": 146,
	"./eu.js": 146,
	"./fa": 147,
	"./fa.js": 147,
	"./fi": 148,
	"./fi.js": 148,
	"./fo": 149,
	"./fo.js": 149,
	"./fr": 152,
	"./fr-ca": 150,
	"./fr-ca.js": 150,
	"./fr-ch": 151,
	"./fr-ch.js": 151,
	"./fr.js": 152,
	"./fy": 153,
	"./fy.js": 153,
	"./gd": 154,
	"./gd.js": 154,
	"./gl": 155,
	"./gl.js": 155,
	"./gom-latn": 156,
	"./gom-latn.js": 156,
	"./he": 157,
	"./he.js": 157,
	"./hi": 158,
	"./hi.js": 158,
	"./hr": 159,
	"./hr.js": 159,
	"./hu": 160,
	"./hu.js": 160,
	"./hy-am": 161,
	"./hy-am.js": 161,
	"./id": 162,
	"./id.js": 162,
	"./is": 163,
	"./is.js": 163,
	"./it": 164,
	"./it.js": 164,
	"./ja": 165,
	"./ja.js": 165,
	"./jv": 166,
	"./jv.js": 166,
	"./ka": 167,
	"./ka.js": 167,
	"./kk": 168,
	"./kk.js": 168,
	"./km": 169,
	"./km.js": 169,
	"./kn": 170,
	"./kn.js": 170,
	"./ko": 171,
	"./ko.js": 171,
	"./ky": 172,
	"./ky.js": 172,
	"./lb": 173,
	"./lb.js": 173,
	"./lo": 174,
	"./lo.js": 174,
	"./lt": 175,
	"./lt.js": 175,
	"./lv": 176,
	"./lv.js": 176,
	"./me": 177,
	"./me.js": 177,
	"./mi": 178,
	"./mi.js": 178,
	"./mk": 179,
	"./mk.js": 179,
	"./ml": 180,
	"./ml.js": 180,
	"./mr": 181,
	"./mr.js": 181,
	"./ms": 183,
	"./ms-my": 182,
	"./ms-my.js": 182,
	"./ms.js": 183,
	"./my": 184,
	"./my.js": 184,
	"./nb": 185,
	"./nb.js": 185,
	"./ne": 186,
	"./ne.js": 186,
	"./nl": 188,
	"./nl-be": 187,
	"./nl-be.js": 187,
	"./nl.js": 188,
	"./nn": 189,
	"./nn.js": 189,
	"./pa-in": 190,
	"./pa-in.js": 190,
	"./pl": 191,
	"./pl.js": 191,
	"./pt": 193,
	"./pt-br": 192,
	"./pt-br.js": 192,
	"./pt.js": 193,
	"./ro": 194,
	"./ro.js": 194,
	"./ru": 195,
	"./ru.js": 195,
	"./sd": 196,
	"./sd.js": 196,
	"./se": 197,
	"./se.js": 197,
	"./si": 198,
	"./si.js": 198,
	"./sk": 199,
	"./sk.js": 199,
	"./sl": 200,
	"./sl.js": 200,
	"./sq": 201,
	"./sq.js": 201,
	"./sr": 203,
	"./sr-cyrl": 202,
	"./sr-cyrl.js": 202,
	"./sr.js": 203,
	"./ss": 204,
	"./ss.js": 204,
	"./sv": 205,
	"./sv.js": 205,
	"./sw": 206,
	"./sw.js": 206,
	"./ta": 207,
	"./ta.js": 207,
	"./te": 208,
	"./te.js": 208,
	"./tet": 209,
	"./tet.js": 209,
	"./th": 210,
	"./th.js": 210,
	"./tl-ph": 211,
	"./tl-ph.js": 211,
	"./tlh": 212,
	"./tlh.js": 212,
	"./tr": 213,
	"./tr.js": 213,
	"./tzl": 214,
	"./tzl.js": 214,
	"./tzm": 216,
	"./tzm-latn": 215,
	"./tzm-latn.js": 215,
	"./tzm.js": 216,
	"./uk": 217,
	"./uk.js": 217,
	"./ur": 218,
	"./ur.js": 218,
	"./uz": 220,
	"./uz-latn": 219,
	"./uz-latn.js": 219,
	"./uz.js": 220,
	"./vi": 221,
	"./vi.js": 221,
	"./x-pseudo": 222,
	"./x-pseudo.js": 222,
	"./yo": 223,
	"./yo.js": 223,
	"./zh-cn": 224,
	"./zh-cn.js": 224,
	"./zh-hk": 225,
	"./zh-hk.js": 225,
	"./zh-tw": 226,
	"./zh-tw.js": 226
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 386;


/***/ }),

/***/ 390:
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n"

/***/ }),

/***/ 391:
/***/ (function(module, exports) {

module.exports = "<header>\n\t<div class=\"container-fluid\" style=\"padding: 0px\">\n\t\t<nav class=\"navbar navbar-default\">\n\t\t\t<div class=\"container-fluid\">\n\t\t\t\t<!-- Brand and toggle get grouped for better mobile display -->\n\t\t\t\t<div class=\"navbar-header \">\n\t\t\t\t\t<button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\" (click)=\"showMenu()\">\n\t\t\t\t        <span class=\"sr-only\">Toggle navigation</span>\n\t\t\t\t        <span class=\"icon-bar\"></span>\n\t\t\t\t        <span class=\"icon-bar\"></span>\n\t\t\t\t        <span class=\"icon-bar\"></span>\n\t\t\t      \t</button>\n\t\t\t\t\t<a class=\"navbar-brand\" href=\"#\" routerLink=\"/\"><img alt=\"\" src=\"assets/images/logo-sm-2.png\"></a>\n\t\t\t\t</div>\n\n\t\t\t\t<!-- Collect the nav links, forms, and other content for toggling -->\n\t\t\t\t<div [class.collapse]=\"isShowMenu\" class=\"navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\n\t\t\t\t\t<ul class=\"nav navbar-nav navbar-right\">\n\t\t\t\t\t\t<li><a href=\"#what-is-investation\">What is InveStation <span class=\"sr-only\">(current)</span></a></li>\n\t\t\t\t\t\t<li><a href=\"#how-it-works\">How it works</a></li>\n\t\t\t\t\t\t<li><a href=\"#meet-the-team\">Meet the Team</a></li>\n\t\t\t\t\t\t<li><a href=\"#contact\">Contact</a></li>\n\t\t\t\t\t</ul>\n\t\t\t\t</div>\n\t\t\t\t<!-- /.navbar-collapse -->\n\t\t\t</div>\n\t\t\t<!-- /.container-fluid -->\n\t\t</nav>\n\t</div>\n\n</header>\n\n<div class=\"header\">\n\t<div class=\"container\">\n\t\t<div class=\"row\">\n\t\t\t<div class=\"col-lg-6 col-centered div-title\">\n\t\t\t\t<h1>InveStation</h1>\n\t\t\t\t<h2>Know More About Lipa City</h2>\n\t\t\t\t<br>\n\t\t\t\t<button class=\"btn btn-default header-button\" routerLink=\"/tweets\">Tweets</button>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n\n<section id=\"what-is-investation\" style=\"\">\n\t<div class=\"container\">\n\t\t<div class=\"row\">\n\t\t\t<div class=\"col-md-10 col-centered section-body\">\n\t\t\t\t<h3 class=\"section-header\">What is InveStation</h3>\n\t\t\t\t<p>\n\t\t\t\t\tWith InveStation, you can know more about the city of Lipa, Batangas. It is a website that serves as a go-to info hub of all the details of Lipa City based on the tweets of its residences and the people who have experienced the city. With this, people can read the tweets and replies. And with that, it will be easy for anyone to see, learn, and decide if Lipa City is good for investing or not.\n\t\t\t\t</p>\n\t\t\t\t<p>\n\t\t\t\t\tWhat’s more fun with this website is that, it is easy to use and accessible via PC, mobile, and tablet. It shows all the tweets and the results of the positive and negative tweets that were filtered from Twitter. You can view all the tweets or view the categorized tweets based on the major parameters that are considered in investing. These parameters are culture, population, resources, education, economy, location, climate, and government all the group of positive tweets or negative tweets. You can also see the statistics of the parameters.\n\t\t\t\t</p>\n\t\t\t\t<p>\n\t\t\t\t\tWith this website, you no longer need to look at different websites or do a research in books, magazines, or go to any social media sites just to know if Lipa City is good for investing or not depending on what area or parameter you need. It is designed to meet your curiosity about Lipa City and help you at your own ease.\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</section>\n<section id=\"how-it-works\">\n\t<div class=\"container\">\n\t\t<div class=\"row\">\n\t\t\t<div class=\"col-md-10 col-centered section-body\">\n\t\t\t\t<h3 class=\"section-header\">How it Works?</h3>\n\t\t\t\t<p>\n\t\t\t\t\tInveStation is a website that gets all the tweets and comments that talks about Lipa City, Batangas. It filters all the tweets on Twitter with the help of Twitter API. It gets all those tweets that have the official hashtag of InveStation which is, #WhatsInLipa. This website uses sentiment analysis on every tweet and calculates if a tweet is positive or negative. It then categorizes the parameter of each tweet if it is about the culture, population, economy, government, resources, location, climate, or education. People can view the filtered tweets and the replies to the corresponding tweets and see if it is good to invest in Lipa City or not. The statistics of the tweets of each parameter can also be viewed. The data is easy to understand and can easily give the users an easy access to review Lipa City.\n\t\t\t\t</p>\n\t\t\t\t<p>\n\t\t\t\t\tSo what are you waiting for? Want to find a good investment? Start searching on InveStation!\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</section>\n<section id=\"parameters\">\n\t<div class=\"\">\n\t\t<div class=\"row\">\n\t\t\t<div class=\"col-md-12 col-centered section-body\">\n\t\t\t\t<h3 class=\"section-header\">Parameters</h3>\n\t\t\t</div>\n\n\t\t\t<div class=\"col-md-8\">\n\t\t\t\t<div class=\"row text-centered H100\">\n\t\t\t\t\t<div class=\"legend\" *ngFor=\"let legend of legends\">\n\t\t\t\t\t\t<div class=\"legend-color legend-{{ legend.title }}\"></div>\n\t\t\t\t\t\t<div class=\"legend-title\"> {{ legend.title }} </div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<div class=\"col-sm-12 col-md-6\">\n\t\t\t\t\t\t<h3>Positive</h3>\n\t\t\t\t\t\t<div style=\"display: block\">\n\t\t\t\t\t\t  <canvas baseChart\n\t\t\t\t\t\t          [data]=\"positivePieChartData\"\n\t\t\t\t\t\t\t\t  [legend]=\"false\"\n\t\t\t\t\t\t          [labels]=\"sentimentPieChartLabels\"\n\t\t\t\t\t\t          [chartType]=\"pieChartType\"\n\t\t\t\t\t\t\t\t  [colors]=\"backgroundColor\"\n\t\t\t\t\t\t          (chartClick)=\"posSentimentPieChartClicked($event)\"></canvas>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"row stats text-centered\">\n\t\t\t\t\t\t\t<b> Education: {{ getParametersPercentage( 'POSITIVE','education', this.tweets.pos ) }} </b>  <br />\n\t\t\t\t\t\t\t<b> Economy: {{ getParametersPercentage( 'POSITIVE','economy', this.tweets.pos ) }} </b>  <br />\n\t\t\t\t\t\t\t<b> Resources: {{ getParametersPercentage( 'POSITIVE','resources', this.tweets.pos ) }} </b>  <br />\n\t\t\t\t\t\t\t<b> Climate: {{ getParametersPercentage( 'POSITIVE','climate', this.tweets.pos ) }} </b>  <br />\n\t\t\t\t\t\t\t<b> Location: {{ getParametersPercentage( 'POSITIVE','location', this.tweets.pos ) }} </b>  <br />\n\t\t\t\t\t\t\t<b> Government: {{ getParametersPercentage( 'POSITIVE','government', this.tweets.pos ) }} </b>  <br />\n\t\t\t\t\t\t\t<b> Culture: {{ getParametersPercentage( 'POSITIVE','culture', this.tweets.pos ) }} </b>  <br />\n\t\t\t\t\t\t\t<b> Population: {{ getParametersPercentage( 'POSITIVE','population', this.tweets.pos ) }} </b>  <br />\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<br />\n\t\t\t\t\t\t<br />\n\t\t\t\t\t\t<br />\n\t\t\t\t\t</div>\n\n\t\t\t\t\t<div class=\"col-sm-12 col-md-6\">\n\t\t\t\t\t\t<h3>Negative</h3>\n\t\t\t\t\t\t<div style=\"display: block\">\n\t\t\t\t\t\t  <canvas baseChart\n\t\t\t\t\t\t          [data]=\"negativePieChartData\"\n\t\t\t\t\t\t\t\t  [legend]=\"false\"\n\t\t\t\t\t\t          [labels]=\"sentimentPieChartLabels\"\n\t\t\t\t\t\t          [chartType]=\"pieChartType\"\n\t\t\t\t\t\t          (chartClick)=\"negSentimentPieChartClicked($event)\"></canvas>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"row stats text-centered\">\n\t\t\t\t\t\t\t<b>Education: {{ getParametersPercentage( 'NEGATIVE','education', this.tweets.pos ) }}</b>  <br />\n\t\t\t\t\t\t\t<b>Economy: {{ getParametersPercentage( 'NEGATIVE','economy', this.tweets.pos ) }}</b>  <br />\n\t\t\t\t\t\t\t<b>Resources: {{ getParametersPercentage( 'NEGATIVE','resources', this.tweets.pos ) }}</b>  <br />\n\t\t\t\t\t\t\t<b>Climate: {{ getParametersPercentage( 'NEGATIVE','climate', this.tweets.pos ) }}</b>  <br />\n\t\t\t\t\t\t\t<b>Location: {{ getParametersPercentage( 'NEGATIVE','location', this.tweets.pos ) }}</b>  <br />\n\t\t\t\t\t\t\t<b>Government: {{ getParametersPercentage( 'NEGATIVE','government', this.tweets.pos ) }}</b>  <br />\n\t\t\t\t\t\t\t<b>Culture: {{ getParametersPercentage( 'NEGATIVE','culture', this.tweets.pos ) }}</b>  <br />\n\t\t\t\t\t\t\t<b>Population: {{ getParametersPercentage( 'NEGATIVE','population', this.tweets.pos ) }}</b>  <br />\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<br />\n\t\t\t\t\t\t<br />\n\t\t\t\t\t\t<br />\n\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div class=\"col-md-4\">\n\t\t\t\t<div class=\"row text-centered H100\">\n\t\t\t\t\t<div class=\"legend\">\n\t\t\t\t\t\t<div class=\"legend-color legend-positive\"></div>\n\t\t\t\t\t\t<div class=\"legend-title\"> Positive </div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"legend\">\n\t\t\t\t\t\t<div class=\"legend-color legend-negative\"></div>\n\t\t\t\t\t\t<div class=\"legend-title\"> Negative </div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<h3>Summary of Tweets</h3>\n\n\t\t\t\t<div style=\"display: block\">\n\t\t\t\t  <canvas baseChart\n\t\t\t\t  \t\t  [legend]=\"false\"\n\t\t\t\t          [data]=\"tweetsPieChartData\"\n\t\t\t\t          [labels]=\"tweetsPieChartLabels\"\n\t\t\t\t          [chartType]=\"pieChartType\"\n\t\t\t\t          (chartClick)=\"totalPieChartClicked($event)\"></canvas>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"row stats text-centered\">\n\t\t\t\t\t<div class=\"col-md-12\">\n\t\t\t\t\t\t<b>Total Number of Positive Tweets: {{ tweets.pos }}</b>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-md-12\">\n\t\t\t\t\t\t<b>Total Number of Negative Tweets: {{ tweets.neg }}</b>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col-md-12\">\n\t\t\t\t\t\t<b>Total Number of all Tweets: {{ tweets.total }}</b>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t\t<br />\n\t\t\t\t<br />\n\t\t\t\t<br />\n\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</section>\n<section id=\"meet-the-team\">\n\t<div class=\"container-fluid\">\n\t\t<div class=\"row center\">\n\t\t\t<div class=\"col-md-10 col-centered section-body\">\n\t\t\t\t<h3 class=\"section-header\">Meet the Team</h3>\n\t\t\t\t<div *ngFor=\"let team of aTeams\" class=\"col-md-4 team-member\">\n\t\t\t\t\t<div class=\"ProfileAvatar\">\n\t\t\t\t\t\t<a class=\"account-group js-account-group js-action-profile js-user-profile-link js-nav\" target=\"_blank\" href=\"https://twitter.com/{{ team.username }}\" data-user-id=\"746717818171756545\">\n\t\t\t\t\t\t\t<img class=\".img-circle\" src=\"https://pbs.twimg.com/profile_images/{{ team.img }}\" alt=\"\">\n\t\t\t\t\t\t</a>\n\t\t\t\t\t</div>\n\t\t\t\t\t<h4 class=\"member-name\">{{ team.sName }}</h4>\n\t\t\t\t\t<span>@{{ team.username }}</span><br>\n\t\t\t\t\t<br>\n\t\t\t\t\t<a href=\"https://twitter.com/{{ team.username }}\" target=\"_tab\" class=\"btn btn-follow\"><i class=\"fa fa-twitter\"></i> Follow @{{ team.username }}</a>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"row\">\n\n\t\t</div>\n\t</div>\n</section>\n<section id=\"contact\">\n\t<div class=\"container-fluid\">\n\t\t<div class=\"row\">\n\t\t\t<div class=\"col-md-8 col-centered section-body\">\n\t\t\t\t<h3 class=\"section-header\">Contact</h3>\n\n\t\t\t\t<div class=\"col-md-4 contact-info\">\n\t\t\t\t\t<i class=\"fa fa-twitter fa-3x\"></i>\n\t\t\t\t\t<h4><b>Twitter</b></h4>\n\t\t\t\t\t<p>http://twitter.com/LipaInvestation</p>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-md-4 contact-info\">\n\t\t\t\t\t<i class=\"fa fa-envelope fa-3x\"></i>\n\t\t\t\t\t<h4><b>Email</b></h4>\n\t\t\t\t\t<p>investationlipacity@gmail.com</p>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-md-4 contact-info\">\n\t\t\t\t\t<i class=\"fa fa-phone fa-3x\"></i>\n\t\t\t\t\t<h4><b>Phone</b></h4>\n\t\t\t\t\t<p>+63 915 288 9194</p>\n\t\t\t\t</div>\n\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</section>\n<footer>\n\t<div class=\"container\">\n\t\t<div class=\"row\">\n\t\t\t<div class=\"col-md-10 col-centered\">\n\t\t\t\t<p class=\"copyright\">&copy; Investation. All rights reserved. 2017</p>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</footer>\n"

/***/ }),

/***/ 392:
/***/ (function(module, exports) {

module.exports = "<div class=\"spinner\">\n\t<div class=\"spinner-content\">\n\t\t<img src=\"assets/images/spinner.gif\">\n\t</div>\n</div>\n"

/***/ }),

/***/ 393:
/***/ (function(module, exports) {

module.exports = "<header>\n\t<div class=\"container-fluid\" style=\"padding: 0px\">\n\t\t<nav class=\"navbar navbar-default\">\n\t\t\t<div class=\"container-fluid\">\n\t\t\t\t<!-- Brand and toggle get grouped for better mobile display -->\n\t\t\t\t<div class=\"navbar-header \">\n\t\t\t\t\t<button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\" (click)=\"showMenu()\">\n\t\t\t\t        <span class=\"sr-only\">Toggle navigation</span>\n\t\t\t\t        <span class=\"icon-bar\"></span>\n\t\t\t\t        <span class=\"icon-bar\"></span>\n\t\t\t\t        <span class=\"icon-bar\"></span>\n\t\t\t      \t</button>\n\t\t\t\t\t<a class=\"navbar-brand\" href=\"#\" routerLink=\"/\"><img alt=\"\" src=\"assets/images/logo-sm-2.png\"></a>\n\t\t\t\t</div>\n\n\t\t\t\t<!-- Collect the nav links, forms, and other content for toggling -->\n\t\t\t\t<div [class.collapse]=\"isShowMenu\" class=\"navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\n\t\t\t\t\t<ul class=\"nav navbar-nav navbar-right\">\n\t\t\t\t\t\t<li><a href=\"javascript:void(0)\" routerLink=\"/\">Home <span class=\"sr-only\">(current)</span></a></li>\n\t\t\t\t\t\t<li><a href=\"javascript:void(0)\" routerLink=\"/tweets\" class=\"active\">Tweets</a></li>\n\t\t\t\t\t</ul>\n\t\t\t\t</div>\n\t\t\t\t<!-- /.navbar-collapse -->\n\t\t\t</div>\n\t\t\t<!-- /.container-fluid -->\n\t\t</nav>\n\t</div>\n\n</header>\n\n<div class=\"container\">\n\n\t<div class=\"row\">\n\t\t<div id=\"tweets-container\">\n\t\t\t<div *ngIf=\"this.tweets\">\n\t\t\t\t<div *ngFor=\"let tweet of tweets\" class=\"tweet col-lg-6\">\n\t\t\t\t\t<div class=\"panel\" id=\"tweet-{{ tweet.id_str }}\">\n\t\t\t\t\t\t<div class=\"panel-heading\">\n\t\t\t\t\t\t\t<div class=\"loader\"></div>\n\t\t\t\t\t\t\tSentiment Analysis: \t<h3 class=\"panel-title {{ tweet.analysis }}\"></h3>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t{{ createTweets(tweet,\"tweet\",last) }}\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div *ngIf=\"!this.tweets\" class=\"\">\n\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n\n<div id=\"replies_modal\" class=\"modal\">\n\t<!-- Modal content -->\n\t<div class=\"modal-content\">\n\t\t<div class=\"panel panel-default\">\n\t\t\t<div class=\"panel-heading\">\n\t\t\t\t<button type=\"button\" class=\"close\" (click)=\"ToggleModal('none')\">&times;</button> Sentiment Analysis\n\t\t\t</div>\n\t\t\t<div class=\"panel-body\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<div class=\"col-md-4\">\n\t\t\t\t\t\t\t<label class=\"control-label\">Choose analysis type</label>\n\t\t\t\t\t\t\t<select id=\"sentiment\" (change)=\"showReplies(null,sentiment.value)\" #sentiment class=\"form-control select\" style=\"width:100%\">\n\t\t\t\t    \t\t\t<option value=\"pos\">Positive</option>\n\t\t\t\t    \t\t\t<option value=\"neg\">Negative</option>\n\t\t\t\t    \t\t</select>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div *ngIf=\"analysis?.length < 1\" class=\"row\">\n\t\t\t\t\t<div class=\"col-md-12\">\n\t\t\t\t\t\t<h4 class=\"text-center\"> No record(s) to show</h4>\n\t\t\t\t\t</div>\n\n\t\t\t\t</div>\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<div id=\"replies\" *ngFor=\"let analysis of analysis; let last = last \" class=\"col-md-6 col-sm-6\">\n\t\t\t\t\t\t<div id=\"analysis-{{ analysis.id_str }}\">{{ createTweets(analysis,\"analysis\",last) }}</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n\n<spinner [ngStyle]=\"{ 'display': bShowLoader ? 'block' : 'none' }\"></spinner>\n"

/***/ }),

/***/ 666:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(267);


/***/ }),

/***/ 67:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HttpService = (function () {
    function HttpService(http) {
        this.http = http;
        console.log(location.hostname);
    }
    HttpService.prototype.getTweets = function (params, sentiment) {
        if (params === void 0) { params = null; }
        if (sentiment === void 0) { sentiment = null; }
        var api = 'http://' + location.hostname + '/investation/api/tweets';
        return this.http.get(api + (sentiment || params ? '?key=' + params + '&sentiment=' + sentiment : ''))
            .map(function (res) { return res.json(); });
    };
    HttpService.prototype.getParameters = function () {
        console.log('http://' + location.hostname + '/investation/api/tweets');
        var api = 'http://' + location.hostname + '/investation/api/tweets';
        return this.http.get(api + '?paramsOnly=true')
            .map(function (res) { return res.json(); });
    };
    return HttpService;
}());
HttpService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object])
], HttpService);

var _a;
//# sourceMappingURL=http.service.js.map

/***/ }),

/***/ 94:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_http_service__ = __webpack_require__(67);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomeComponent = (function () {
    function HomeComponent(HttpService, router) {
        this.HttpService = HttpService;
        this.router = router;
        this.sentimentPieChartLabels = [
            'education',
            'economy',
            'resources',
            'climate',
            'location',
            'government',
            'culture',
            'population'
        ];
        this.tweetsPieChartLabels = [
            'negative',
            'positive'
        ];
        this.negativePieChartData = [1, 1, 1, 1, 1, 1, 1, 1];
        this.positivePieChartData = [1, 1, 1, 1, 1, 1, 1, 1];
        this.tweetsPieChartData = [1, 1];
        this.pieChartType = 'pie';
        this.tweets = [];
        this.isShowMenu = true;
    }
    HomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.title = 'InveStation';
        this.aTeams = [
            { sName: 'Alex On Lojo', username: 'mayhem_blue', img: '640810037317767168/MUZ98Jag_400x400.jpg' },
            { sName: 'Derick Bautista', username: 'IncredibleDrK', img: '858197538792394752/5q4GAmZ__400x400.jpg' },
            { sName: 'Kristian Dave Cruz Pelegrino', username: 'kdpelegrino', img: '886811162574168064/CNx43M6e_400x400.jpg' },
        ];
        this.legends = [
            { title: 'education', color: '#ffa2b5' },
            { title: 'economy', color: '#8bc7f3' },
            { title: 'resources', color: '#ffe29d' },
            { title: 'climate', color: '#f1f2f4' },
            { title: 'location', color: '#98dad9' },
            { title: 'government', color: '#c1d6e1' },
            { title: 'culture', color: '#eaeaea' },
            { title: 'population', color: '#fd9393' }
        ];
        this.HttpService.getParameters()
            .subscribe(function (data) {
            if (data[0]['aItem']) {
                _this.tweets = JSON.parse(JSON.stringify(data[0]['aItem']));
                _this.positivePieChartData = _this.setParamsData(_this.tweets['params'].POSITIVE);
                _this.negativePieChartData = _this.setParamsData(_this.tweets['params'].NEGATIVE);
                _this.tweetsPieChartData = [_this.tweets['neg'], _this.tweets['pos']];
            }
        });
    };
    HomeComponent.prototype.setParamsData = function (params) {
        return [params.education, params.economy, params.resources, params.climate, params.location, params.government, params.culture, params.population];
    };
    HomeComponent.prototype.posSentimentPieChartClicked = function (e) {
        this.searchTweet(this.sentimentPieChartLabels[e.active[0]._index], 'POSITIVE');
    };
    HomeComponent.prototype.negSentimentPieChartClicked = function (e) {
        this.searchTweet(this.sentimentPieChartLabels[e.active[0]._index], 'NEGATIVE');
    };
    HomeComponent.prototype.totalPieChartClicked = function (e) {
        this.searchTweet('', (this.tweetsPieChartLabels[e.active[0]._index]).toUpperCase());
    };
    HomeComponent.prototype.getParametersPercentage = function (a, b, total) {
        return ((this.tweets['params'][a][b] / total) * 100).toFixed(2) + '%';
    };
    HomeComponent.prototype.searchTweet = function (params, sentiment) {
        this.router.navigate(['/tweets', params, sentiment]);
    };
    HomeComponent.prototype.showMenu = function () {
        this.isShowMenu = !this.isShowMenu;
    };
    return HomeComponent;
}());
HomeComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-home',
        template: __webpack_require__(391),
        styles: [__webpack_require__(382)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__services_http_service__["a" /* HttpService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_http_service__["a" /* HttpService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _b || Object])
], HomeComponent);

var _a, _b;
//# sourceMappingURL=home.component.js.map

/***/ }),

/***/ 95:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(12);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WindowReferenceService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

function _window() {
    // return the global native browser window object
    return window;
}
var WindowReferenceService = (function () {
    function WindowReferenceService() {
    }
    Object.defineProperty(WindowReferenceService.prototype, "nativeWindow", {
        get: function () {
            return _window();
        },
        enumerable: true,
        configurable: true
    });
    return WindowReferenceService;
}());
WindowReferenceService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
], WindowReferenceService);

//# sourceMappingURL=windowreference.service.js.map

/***/ }),

/***/ 96:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_http_service__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_windowreference_service__ = __webpack_require__(95);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TweetsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TweetsComponent = (function () {
    function TweetsComponent(HttpService, windowReferenceService, activatedRoute) {
        this.HttpService = HttpService;
        this.windowReferenceService = windowReferenceService;
        this.activatedRoute = activatedRoute;
        this.winRef = null;
        this.bShowLoader = true;
        this.isShowMenu = true;
        this.winRef = windowReferenceService.nativeWindow;
        this.id_str = null;
    }
    TweetsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (params) {
            _this.getTweets(params.params, params.sentiment);
        });
    };
    TweetsComponent.prototype.createTweets = function (tweet, elementId, last) {
        var _this = this;
        if (tweet.bRendered !== true) {
            this.winRef.twttr.ready(function () {
                var container = document.getElementById(elementId + '-' + tweet.id_str);
                tweet.bRendered !== true &&
                    _this.winRef.twttr.widgets.createTweet(tweet.id_str, container)
                        .then(function (element) {
                        var embeddedTweet = document.querySelector('#' + elementId + '-' + tweet.id_str + '.panel .panel-title');
                        var twitterwidget = document.querySelector('#' + elementId + '-' + tweet.id_str + ' twitterwidget');
                        var loader = document.querySelector('#' + elementId + '-' + tweet.id_str + ' .loader');
                        if (embeddedTweet) {
                            embeddedTweet.innerHTML = tweet.analysis;
                            loader.style.visibility = 'hidden';
                        }
                        container && (container.style.display = twitterwidget.style.visibility == 'hidden' ? 'none' : 'inline-block');
                    });
                tweet.bRendered = true;
            });
        }
    };
    TweetsComponent.prototype.getTweets = function (key, sentiment) {
        var _this = this;
        this.HttpService.getTweets(key, sentiment)
            .subscribe(function (data) {
            if (data[0]['aItem']['tweets']) {
                _this.tweets = JSON.parse(JSON.stringify(data[0]['aItem']['tweets']));
            }
            _this.hideLoader();
            console.log(_this.tweets);
        });
    };
    TweetsComponent.prototype.ToggleModal = function (state) {
        console.log(this.analysis);
        // let display = document.getElementById('replies_modal').style.display;
        document.getElementById('replies_modal').style.display = state;
    };
    TweetsComponent.prototype.hideLoader = function () {
        this.bShowLoader = false;
    };
    TweetsComponent.prototype.sentimenBorderColor = function (tweet) {
        if (tweet.analysis != '') {
            return tweet.analysis == 'pos' ? 'green' : 'red';
        }
        else {
            return 'gray';
        }
    };
    TweetsComponent.prototype.showMenu = function () {
        this.isShowMenu = !this.isShowMenu;
    };
    return TweetsComponent;
}());
TweetsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-tweets',
        template: __webpack_require__(393),
        styles: [__webpack_require__(384)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_http_service__["a" /* HttpService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_http_service__["a" /* HttpService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__services_windowreference_service__["a" /* WindowReferenceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_windowreference_service__["a" /* WindowReferenceService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* ActivatedRoute */]) === "function" && _c || Object])
], TweetsComponent);

var _a, _b, _c;
//# sourceMappingURL=tweets.component.js.map

/***/ })

},[666]);
//# sourceMappingURL=main.bundle.js.map
