webpackJsonp([3,5],{

/***/ 269:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(660)(__webpack_require__(389))

/***/ }),

/***/ 389:
/***/ (function(module, exports) {

module.exports = "/**\n * @namespace Chart\n */\nvar Chart = require('./core/core')();\n\nChart.helpers = require('./helpers/index');\n\n// @todo dispatch these helpers into appropriated helpers/helpers.* file and write unit tests!\nrequire('./core/core.helpers')(Chart);\n\nChart.defaults = require('./core/core.defaults');\nChart.Element = require('./core/core.element');\nChart.elements = require('./elements/index');\nChart.Interaction = require('./core/core.interaction');\nChart.platform = require('./platforms/platform');\n\nrequire('./core/core.plugin')(Chart);\nrequire('./core/core.animation')(Chart);\nrequire('./core/core.controller')(Chart);\nrequire('./core/core.datasetController')(Chart);\nrequire('./core/core.layoutService')(Chart);\nrequire('./core/core.scaleService')(Chart);\nrequire('./core/core.scale')(Chart);\nrequire('./core/core.tooltip')(Chart);\n\nrequire('./scales/scale.linearbase')(Chart);\nrequire('./scales/scale.category')(Chart);\nrequire('./scales/scale.linear')(Chart);\nrequire('./scales/scale.logarithmic')(Chart);\nrequire('./scales/scale.radialLinear')(Chart);\nrequire('./scales/scale.time')(Chart);\n\n// Controllers must be loaded after elements\n// See Chart.core.datasetController.dataElementType\nrequire('./controllers/controller.bar')(Chart);\nrequire('./controllers/controller.bubble')(Chart);\nrequire('./controllers/controller.doughnut')(Chart);\nrequire('./controllers/controller.line')(Chart);\nrequire('./controllers/controller.polarArea')(Chart);\nrequire('./controllers/controller.radar')(Chart);\nrequire('./controllers/controller.scatter')(Chart);\n\nrequire('./charts/Chart.Bar')(Chart);\nrequire('./charts/Chart.Bubble')(Chart);\nrequire('./charts/Chart.Doughnut')(Chart);\nrequire('./charts/Chart.Line')(Chart);\nrequire('./charts/Chart.PolarArea')(Chart);\nrequire('./charts/Chart.Radar')(Chart);\nrequire('./charts/Chart.Scatter')(Chart);\n\n// Loading built-it plugins\nvar plugins = [];\n\nplugins.push(\n\trequire('./plugins/plugin.filler')(Chart),\n\trequire('./plugins/plugin.legend')(Chart),\n\trequire('./plugins/plugin.title')(Chart)\n);\n\nChart.plugins.register(plugins);\n\nChart.platform.initialize();\n\nmodule.exports = Chart;\nif (typeof window !== 'undefined') {\n\twindow.Chart = Chart;\n}\n\n// DEPRECATIONS\n\n/**\n * Provided for backward compatibility, use Chart.helpers.canvas instead.\n * @namespace Chart.canvasHelpers\n * @deprecated since version 2.6.0\n * @todo remove at version 3\n * @private\n */\nChart.canvasHelpers = Chart.helpers.canvas;\n"

/***/ }),

/***/ 660:
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
module.exports = function(src) {
	if (typeof execScript !== "undefined")
		execScript(src);
	else
		eval.call(null, src);
}


/***/ }),

/***/ 669:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(269);


/***/ })

},[669]);
//# sourceMappingURL=scripts.bundle.js.map